package org.mgoulene.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class RealEstateItemTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static RealEstateItem getRealEstateItemSample1() {
        return new RealEstateItem().id(1L);
    }

    public static RealEstateItem getRealEstateItemSample2() {
        return new RealEstateItem().id(2L);
    }

    public static RealEstateItem getRealEstateItemRandomSampleGenerator() {
        return new RealEstateItem().id(longCount.incrementAndGet());
    }
}
