package org.mgoulene.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.domain.ApplicationUserTestSamples.*;
import static org.mgoulene.domain.BankAccountTestSamples.*;
import static org.mgoulene.domain.BudgetItemPeriodTestSamples.*;
import static org.mgoulene.domain.OperationTestSamples.*;
import static org.mgoulene.domain.SubCategoryTestSamples.*;

import org.junit.jupiter.api.Test;
import org.mgoulene.web.rest.TestUtil;

class OperationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Operation.class);
        Operation operation1 = getOperationSample1();
        Operation operation2 = new Operation();
        assertThat(operation1).isNotEqualTo(operation2);

        operation2.setId(operation1.getId());
        assertThat(operation1).isEqualTo(operation2);

        operation2 = getOperationSample2();
        assertThat(operation1).isNotEqualTo(operation2);
    }

    @Test
    void subCategoryTest() throws Exception {
        Operation operation = getOperationRandomSampleGenerator();
        SubCategory subCategoryBack = getSubCategoryRandomSampleGenerator();

        operation.setSubCategory(subCategoryBack);
        assertThat(operation.getSubCategory()).isEqualTo(subCategoryBack);

        operation.subCategory(null);
        assertThat(operation.getSubCategory()).isNull();
    }

    @Test
    void accountTest() throws Exception {
        Operation operation = getOperationRandomSampleGenerator();
        ApplicationUser applicationUserBack = getApplicationUserRandomSampleGenerator();

        operation.setAccount(applicationUserBack);
        assertThat(operation.getAccount()).isEqualTo(applicationUserBack);

        operation.account(null);
        assertThat(operation.getAccount()).isNull();
    }

    @Test
    void bankAccountTest() throws Exception {
        Operation operation = getOperationRandomSampleGenerator();
        BankAccount bankAccountBack = getBankAccountRandomSampleGenerator();

        operation.setBankAccount(bankAccountBack);
        assertThat(operation.getBankAccount()).isEqualTo(bankAccountBack);

        operation.bankAccount(null);
        assertThat(operation.getBankAccount()).isNull();
    }

    @Test
    void budgetItemPeriodTest() throws Exception {
        Operation operation = getOperationRandomSampleGenerator();
        BudgetItemPeriod budgetItemPeriodBack = getBudgetItemPeriodRandomSampleGenerator();

        operation.setBudgetItemPeriod(budgetItemPeriodBack);
        assertThat(operation.getBudgetItemPeriod()).isEqualTo(budgetItemPeriodBack);
        assertThat(budgetItemPeriodBack.getOperation()).isEqualTo(operation);

        operation.budgetItemPeriod(null);
        assertThat(operation.getBudgetItemPeriod()).isNull();
        assertThat(budgetItemPeriodBack.getOperation()).isNull();
    }
}
