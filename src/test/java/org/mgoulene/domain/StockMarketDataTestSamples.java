package org.mgoulene.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class StockMarketDataTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static StockMarketData getStockMarketDataSample1() {
        return new StockMarketData().id(1L).symbol("symbol1");
    }

    public static StockMarketData getStockMarketDataSample2() {
        return new StockMarketData().id(2L).symbol("symbol2");
    }

    public static StockMarketData getStockMarketDataRandomSampleGenerator() {
        return new StockMarketData().id(longCount.incrementAndGet()).symbol(UUID.randomUUID().toString());
    }
}
