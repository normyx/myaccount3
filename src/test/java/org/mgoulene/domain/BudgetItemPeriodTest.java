package org.mgoulene.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.domain.BudgetItemPeriodTestSamples.*;
import static org.mgoulene.domain.BudgetItemTestSamples.*;
import static org.mgoulene.domain.OperationTestSamples.*;

import org.junit.jupiter.api.Test;
import org.mgoulene.web.rest.TestUtil;

class BudgetItemPeriodTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BudgetItemPeriod.class);
        BudgetItemPeriod budgetItemPeriod1 = getBudgetItemPeriodSample1();
        BudgetItemPeriod budgetItemPeriod2 = new BudgetItemPeriod();
        assertThat(budgetItemPeriod1).isNotEqualTo(budgetItemPeriod2);

        budgetItemPeriod2.setId(budgetItemPeriod1.getId());
        assertThat(budgetItemPeriod1).isEqualTo(budgetItemPeriod2);

        budgetItemPeriod2 = getBudgetItemPeriodSample2();
        assertThat(budgetItemPeriod1).isNotEqualTo(budgetItemPeriod2);
    }

    @Test
    void operationTest() throws Exception {
        BudgetItemPeriod budgetItemPeriod = getBudgetItemPeriodRandomSampleGenerator();
        Operation operationBack = getOperationRandomSampleGenerator();

        budgetItemPeriod.setOperation(operationBack);
        assertThat(budgetItemPeriod.getOperation()).isEqualTo(operationBack);

        budgetItemPeriod.operation(null);
        assertThat(budgetItemPeriod.getOperation()).isNull();
    }

    @Test
    void budgetItemTest() throws Exception {
        BudgetItemPeriod budgetItemPeriod = getBudgetItemPeriodRandomSampleGenerator();
        BudgetItem budgetItemBack = getBudgetItemRandomSampleGenerator();

        budgetItemPeriod.setBudgetItem(budgetItemBack);
        assertThat(budgetItemPeriod.getBudgetItem()).isEqualTo(budgetItemBack);

        budgetItemPeriod.budgetItem(null);
        assertThat(budgetItemPeriod.getBudgetItem()).isNull();
    }
}
