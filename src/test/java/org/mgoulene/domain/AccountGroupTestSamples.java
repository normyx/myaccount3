package org.mgoulene.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class AccountGroupTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static AccountGroup getAccountGroupSample1() {
        return new AccountGroup().id(1L).groupName("groupName1");
    }

    public static AccountGroup getAccountGroupSample2() {
        return new AccountGroup().id(2L).groupName("groupName2");
    }

    public static AccountGroup getAccountGroupRandomSampleGenerator() {
        return new AccountGroup().id(longCount.incrementAndGet()).groupName(UUID.randomUUID().toString());
    }
}
