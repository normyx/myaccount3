package org.mgoulene.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.domain.AccountGroupTestSamples.*;
import static org.mgoulene.domain.ApplicationUserTestSamples.*;
import static org.mgoulene.domain.BankAccountTestSamples.*;
import static org.mgoulene.domain.RealEstateItemTestSamples.*;
import static org.mgoulene.domain.StockPortfolioItemTestSamples.*;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.mgoulene.web.rest.TestUtil;

class BankAccountTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankAccount.class);
        BankAccount bankAccount1 = getBankAccountSample1();
        BankAccount bankAccount2 = new BankAccount();
        assertThat(bankAccount1).isNotEqualTo(bankAccount2);

        bankAccount2.setId(bankAccount1.getId());
        assertThat(bankAccount1).isEqualTo(bankAccount2);

        bankAccount2 = getBankAccountSample2();
        assertThat(bankAccount1).isNotEqualTo(bankAccount2);
    }

    @Test
    void realEstateItemTest() throws Exception {
        BankAccount bankAccount = getBankAccountRandomSampleGenerator();
        RealEstateItem realEstateItemBack = getRealEstateItemRandomSampleGenerator();

        bankAccount.addRealEstateItem(realEstateItemBack);
        assertThat(bankAccount.getRealEstateItems()).containsOnly(realEstateItemBack);
        assertThat(realEstateItemBack.getBankAccount()).isEqualTo(bankAccount);

        bankAccount.removeRealEstateItem(realEstateItemBack);
        assertThat(bankAccount.getRealEstateItems()).doesNotContain(realEstateItemBack);
        assertThat(realEstateItemBack.getBankAccount()).isNull();

        bankAccount.realEstateItems(new HashSet<>(Set.of(realEstateItemBack)));
        assertThat(bankAccount.getRealEstateItems()).containsOnly(realEstateItemBack);
        assertThat(realEstateItemBack.getBankAccount()).isEqualTo(bankAccount);

        bankAccount.setRealEstateItems(new HashSet<>());
        assertThat(bankAccount.getRealEstateItems()).doesNotContain(realEstateItemBack);
        assertThat(realEstateItemBack.getBankAccount()).isNull();
    }

    @Test
    void stockPortfolioItemTest() throws Exception {
        BankAccount bankAccount = getBankAccountRandomSampleGenerator();
        StockPortfolioItem stockPortfolioItemBack = getStockPortfolioItemRandomSampleGenerator();

        bankAccount.addStockPortfolioItem(stockPortfolioItemBack);
        assertThat(bankAccount.getStockPortfolioItems()).containsOnly(stockPortfolioItemBack);
        assertThat(stockPortfolioItemBack.getBankAccount()).isEqualTo(bankAccount);

        bankAccount.removeStockPortfolioItem(stockPortfolioItemBack);
        assertThat(bankAccount.getStockPortfolioItems()).doesNotContain(stockPortfolioItemBack);
        assertThat(stockPortfolioItemBack.getBankAccount()).isNull();

        bankAccount.stockPortfolioItems(new HashSet<>(Set.of(stockPortfolioItemBack)));
        assertThat(bankAccount.getStockPortfolioItems()).containsOnly(stockPortfolioItemBack);
        assertThat(stockPortfolioItemBack.getBankAccount()).isEqualTo(bankAccount);

        bankAccount.setStockPortfolioItems(new HashSet<>());
        assertThat(bankAccount.getStockPortfolioItems()).doesNotContain(stockPortfolioItemBack);
        assertThat(stockPortfolioItemBack.getBankAccount()).isNull();
    }

    @Test
    void accountTest() throws Exception {
        BankAccount bankAccount = getBankAccountRandomSampleGenerator();
        ApplicationUser applicationUserBack = getApplicationUserRandomSampleGenerator();

        bankAccount.setAccount(applicationUserBack);
        assertThat(bankAccount.getAccount()).isEqualTo(applicationUserBack);

        bankAccount.account(null);
        assertThat(bankAccount.getAccount()).isNull();
    }

    @Test
    void accountGroupTest() throws Exception {
        BankAccount bankAccount = getBankAccountRandomSampleGenerator();
        AccountGroup accountGroupBack = getAccountGroupRandomSampleGenerator();

        bankAccount.addAccountGroup(accountGroupBack);
        assertThat(bankAccount.getAccountGroups()).containsOnly(accountGroupBack);
        assertThat(accountGroupBack.getBankAccounts()).containsOnly(bankAccount);

        bankAccount.removeAccountGroup(accountGroupBack);
        assertThat(bankAccount.getAccountGroups()).doesNotContain(accountGroupBack);
        assertThat(accountGroupBack.getBankAccounts()).doesNotContain(bankAccount);

        bankAccount.accountGroups(new HashSet<>(Set.of(accountGroupBack)));
        assertThat(bankAccount.getAccountGroups()).containsOnly(accountGroupBack);
        assertThat(accountGroupBack.getBankAccounts()).containsOnly(bankAccount);

        bankAccount.setAccountGroups(new HashSet<>());
        assertThat(bankAccount.getAccountGroups()).doesNotContain(accountGroupBack);
        assertThat(accountGroupBack.getBankAccounts()).doesNotContain(bankAccount);
    }
}
