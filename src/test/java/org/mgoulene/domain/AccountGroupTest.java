package org.mgoulene.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.domain.AccountGroupTestSamples.*;
import static org.mgoulene.domain.ApplicationUserTestSamples.*;
import static org.mgoulene.domain.BankAccountTestSamples.*;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.mgoulene.web.rest.TestUtil;

class AccountGroupTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountGroup.class);
        AccountGroup accountGroup1 = getAccountGroupSample1();
        AccountGroup accountGroup2 = new AccountGroup();
        assertThat(accountGroup1).isNotEqualTo(accountGroup2);

        accountGroup2.setId(accountGroup1.getId());
        assertThat(accountGroup1).isEqualTo(accountGroup2);

        accountGroup2 = getAccountGroupSample2();
        assertThat(accountGroup1).isNotEqualTo(accountGroup2);
    }

    @Test
    void accountTest() throws Exception {
        AccountGroup accountGroup = getAccountGroupRandomSampleGenerator();
        ApplicationUser applicationUserBack = getApplicationUserRandomSampleGenerator();

        accountGroup.setAccount(applicationUserBack);
        assertThat(accountGroup.getAccount()).isEqualTo(applicationUserBack);

        accountGroup.account(null);
        assertThat(accountGroup.getAccount()).isNull();
    }

    @Test
    void bankAccountTest() throws Exception {
        AccountGroup accountGroup = getAccountGroupRandomSampleGenerator();
        BankAccount bankAccountBack = getBankAccountRandomSampleGenerator();

        accountGroup.addBankAccount(bankAccountBack);
        assertThat(accountGroup.getBankAccounts()).containsOnly(bankAccountBack);

        accountGroup.removeBankAccount(bankAccountBack);
        assertThat(accountGroup.getBankAccounts()).doesNotContain(bankAccountBack);

        accountGroup.bankAccounts(new HashSet<>(Set.of(bankAccountBack)));
        assertThat(accountGroup.getBankAccounts()).containsOnly(bankAccountBack);

        accountGroup.setBankAccounts(new HashSet<>());
        assertThat(accountGroup.getBankAccounts()).doesNotContain(bankAccountBack);
    }
}
