package org.mgoulene.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.domain.ApplicationUserTestSamples.*;
import static org.mgoulene.domain.BudgetItemPeriodTestSamples.*;
import static org.mgoulene.domain.BudgetItemTestSamples.*;
import static org.mgoulene.domain.CategoryTestSamples.*;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.mgoulene.web.rest.TestUtil;

class BudgetItemTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BudgetItem.class);
        BudgetItem budgetItem1 = getBudgetItemSample1();
        BudgetItem budgetItem2 = new BudgetItem();
        assertThat(budgetItem1).isNotEqualTo(budgetItem2);

        budgetItem2.setId(budgetItem1.getId());
        assertThat(budgetItem1).isEqualTo(budgetItem2);

        budgetItem2 = getBudgetItemSample2();
        assertThat(budgetItem1).isNotEqualTo(budgetItem2);
    }

    @Test
    void budgetItemPeriodsTest() throws Exception {
        BudgetItem budgetItem = getBudgetItemRandomSampleGenerator();
        BudgetItemPeriod budgetItemPeriodBack = getBudgetItemPeriodRandomSampleGenerator();

        budgetItem.addBudgetItemPeriods(budgetItemPeriodBack);
        assertThat(budgetItem.getBudgetItemPeriods()).containsOnly(budgetItemPeriodBack);
        assertThat(budgetItemPeriodBack.getBudgetItem()).isEqualTo(budgetItem);

        budgetItem.removeBudgetItemPeriods(budgetItemPeriodBack);
        assertThat(budgetItem.getBudgetItemPeriods()).doesNotContain(budgetItemPeriodBack);
        assertThat(budgetItemPeriodBack.getBudgetItem()).isNull();

        budgetItem.budgetItemPeriods(new HashSet<>(Set.of(budgetItemPeriodBack)));
        assertThat(budgetItem.getBudgetItemPeriods()).containsOnly(budgetItemPeriodBack);
        assertThat(budgetItemPeriodBack.getBudgetItem()).isEqualTo(budgetItem);

        budgetItem.setBudgetItemPeriods(new HashSet<>());
        assertThat(budgetItem.getBudgetItemPeriods()).doesNotContain(budgetItemPeriodBack);
        assertThat(budgetItemPeriodBack.getBudgetItem()).isNull();
    }

    @Test
    void categoryTest() throws Exception {
        BudgetItem budgetItem = getBudgetItemRandomSampleGenerator();
        Category categoryBack = getCategoryRandomSampleGenerator();

        budgetItem.setCategory(categoryBack);
        assertThat(budgetItem.getCategory()).isEqualTo(categoryBack);

        budgetItem.category(null);
        assertThat(budgetItem.getCategory()).isNull();
    }

    @Test
    void accountTest() throws Exception {
        BudgetItem budgetItem = getBudgetItemRandomSampleGenerator();
        ApplicationUser applicationUserBack = getApplicationUserRandomSampleGenerator();

        budgetItem.setAccount(applicationUserBack);
        assertThat(budgetItem.getAccount()).isEqualTo(applicationUserBack);

        budgetItem.account(null);
        assertThat(budgetItem.getAccount()).isNull();
    }
}
