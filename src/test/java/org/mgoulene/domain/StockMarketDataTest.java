package org.mgoulene.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.domain.StockMarketDataTestSamples.*;

import org.junit.jupiter.api.Test;
import org.mgoulene.web.rest.TestUtil;

class StockMarketDataTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StockMarketData.class);
        StockMarketData stockMarketData1 = getStockMarketDataSample1();
        StockMarketData stockMarketData2 = new StockMarketData();
        assertThat(stockMarketData1).isNotEqualTo(stockMarketData2);

        stockMarketData2.setId(stockMarketData1.getId());
        assertThat(stockMarketData1).isEqualTo(stockMarketData2);

        stockMarketData2 = getStockMarketDataSample2();
        assertThat(stockMarketData1).isNotEqualTo(stockMarketData2);
    }
}
