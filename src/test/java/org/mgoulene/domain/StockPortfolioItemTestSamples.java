package org.mgoulene.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class StockPortfolioItemTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static StockPortfolioItem getStockPortfolioItemSample1() {
        return new StockPortfolioItem().id(1L).stockSymbol("stockSymbol1");
    }

    public static StockPortfolioItem getStockPortfolioItemSample2() {
        return new StockPortfolioItem().id(2L).stockSymbol("stockSymbol2");
    }

    public static StockPortfolioItem getStockPortfolioItemRandomSampleGenerator() {
        return new StockPortfolioItem().id(longCount.incrementAndGet()).stockSymbol(UUID.randomUUID().toString());
    }
}
