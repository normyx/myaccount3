package org.mgoulene.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.domain.BankAccountTestSamples.*;
import static org.mgoulene.domain.RealEstateItemTestSamples.*;

import org.junit.jupiter.api.Test;
import org.mgoulene.web.rest.TestUtil;

class RealEstateItemTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RealEstateItem.class);
        RealEstateItem realEstateItem1 = getRealEstateItemSample1();
        RealEstateItem realEstateItem2 = new RealEstateItem();
        assertThat(realEstateItem1).isNotEqualTo(realEstateItem2);

        realEstateItem2.setId(realEstateItem1.getId());
        assertThat(realEstateItem1).isEqualTo(realEstateItem2);

        realEstateItem2 = getRealEstateItemSample2();
        assertThat(realEstateItem1).isNotEqualTo(realEstateItem2);
    }

    @Test
    void bankAccountTest() throws Exception {
        RealEstateItem realEstateItem = getRealEstateItemRandomSampleGenerator();
        BankAccount bankAccountBack = getBankAccountRandomSampleGenerator();

        realEstateItem.setBankAccount(bankAccountBack);
        assertThat(realEstateItem.getBankAccount()).isEqualTo(bankAccountBack);

        realEstateItem.bankAccount(null);
        assertThat(realEstateItem.getBankAccount()).isNull();
    }
}
