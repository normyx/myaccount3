package org.mgoulene.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class BudgetItemTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static BudgetItem getBudgetItemSample1() {
        return new BudgetItem().id(1L).name("name1").order(1);
    }

    public static BudgetItem getBudgetItemSample2() {
        return new BudgetItem().id(2L).name("name2").order(2);
    }

    public static BudgetItem getBudgetItemRandomSampleGenerator() {
        return new BudgetItem().id(longCount.incrementAndGet()).name(UUID.randomUUID().toString()).order(intCount.incrementAndGet());
    }
}
