package org.mgoulene.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class BudgetItemPeriodTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static BudgetItemPeriod getBudgetItemPeriodSample1() {
        return new BudgetItemPeriod().id(1L);
    }

    public static BudgetItemPeriod getBudgetItemPeriodSample2() {
        return new BudgetItemPeriod().id(2L);
    }

    public static BudgetItemPeriod getBudgetItemPeriodRandomSampleGenerator() {
        return new BudgetItemPeriod().id(longCount.incrementAndGet());
    }
}
