package org.mgoulene.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mgoulene.domain.BankAccountTestSamples.*;
import static org.mgoulene.domain.StockPortfolioItemTestSamples.*;

import org.junit.jupiter.api.Test;
import org.mgoulene.web.rest.TestUtil;

class StockPortfolioItemTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StockPortfolioItem.class);
        StockPortfolioItem stockPortfolioItem1 = getStockPortfolioItemSample1();
        StockPortfolioItem stockPortfolioItem2 = new StockPortfolioItem();
        assertThat(stockPortfolioItem1).isNotEqualTo(stockPortfolioItem2);

        stockPortfolioItem2.setId(stockPortfolioItem1.getId());
        assertThat(stockPortfolioItem1).isEqualTo(stockPortfolioItem2);

        stockPortfolioItem2 = getStockPortfolioItemSample2();
        assertThat(stockPortfolioItem1).isNotEqualTo(stockPortfolioItem2);
    }

    @Test
    void bankAccountTest() throws Exception {
        StockPortfolioItem stockPortfolioItem = getStockPortfolioItemRandomSampleGenerator();
        BankAccount bankAccountBack = getBankAccountRandomSampleGenerator();

        stockPortfolioItem.setBankAccount(bankAccountBack);
        assertThat(stockPortfolioItem.getBankAccount()).isEqualTo(bankAccountBack);

        stockPortfolioItem.bankAccount(null);
        assertThat(stockPortfolioItem.getBankAccount()).isNull();
    }
}
