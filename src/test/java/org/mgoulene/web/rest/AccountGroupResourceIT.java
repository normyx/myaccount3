package org.mgoulene.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import jakarta.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.collections4.IterableUtils;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mgoulene.IntegrationTest;
import org.mgoulene.domain.AccountGroup;
import org.mgoulene.domain.ApplicationUser;
import org.mgoulene.domain.BankAccount;
import org.mgoulene.repository.AccountGroupRepository;
import org.mgoulene.repository.search.AccountGroupSearchRepository;
import org.mgoulene.service.AccountGroupService;
import org.mgoulene.service.dto.AccountGroupDTO;
import org.mgoulene.service.mapper.AccountGroupMapper;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AccountGroupResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class AccountGroupResourceIT {

    private static final String DEFAULT_GROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_GROUP_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_DEFAULT_ACTIVE = true;

    private static final String ENTITY_API_URL = "/api/account-groups";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/account-groups/_search";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AccountGroupRepository accountGroupRepository;

    @Mock
    private AccountGroupRepository accountGroupRepositoryMock;

    @Autowired
    private AccountGroupMapper accountGroupMapper;

    @Mock
    private AccountGroupService accountGroupServiceMock;

    @Autowired
    private AccountGroupSearchRepository accountGroupSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAccountGroupMockMvc;

    private AccountGroup accountGroup;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccountGroup createEntity(EntityManager em) {
        AccountGroup accountGroup = new AccountGroup().groupName(DEFAULT_GROUP_NAME).defaultActive(DEFAULT_DEFAULT_ACTIVE);
        // Add required entity
        ApplicationUser applicationUser;
        if (TestUtil.findAll(em, ApplicationUser.class).isEmpty()) {
            applicationUser = ApplicationUserResourceIT.createEntity(em);
            em.persist(applicationUser);
            em.flush();
        } else {
            applicationUser = TestUtil.findAll(em, ApplicationUser.class).get(0);
        }
        accountGroup.setAccount(applicationUser);
        return accountGroup;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccountGroup createUpdatedEntity(EntityManager em) {
        AccountGroup accountGroup = new AccountGroup().groupName(UPDATED_GROUP_NAME).defaultActive(UPDATED_DEFAULT_ACTIVE);
        // Add required entity
        ApplicationUser applicationUser;
        if (TestUtil.findAll(em, ApplicationUser.class).isEmpty()) {
            applicationUser = ApplicationUserResourceIT.createUpdatedEntity(em);
            em.persist(applicationUser);
            em.flush();
        } else {
            applicationUser = TestUtil.findAll(em, ApplicationUser.class).get(0);
        }
        accountGroup.setAccount(applicationUser);
        return accountGroup;
    }

    @AfterEach
    public void cleanupElasticSearchRepository() {
        accountGroupSearchRepository.deleteAll();
        assertThat(accountGroupSearchRepository.count()).isEqualTo(0);
    }

    @BeforeEach
    public void initTest() {
        accountGroup = createEntity(em);
    }

    @Test
    @Transactional
    void createAccountGroup() throws Exception {
        int databaseSizeBeforeCreate = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        // Create the AccountGroup
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);
        restAccountGroupMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isCreated());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeCreate + 1);
        await()
            .atMost(5, TimeUnit.SECONDS)
            .untilAsserted(() -> {
                int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
                assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore + 1);
            });
        AccountGroup testAccountGroup = accountGroupList.get(accountGroupList.size() - 1);
        assertThat(testAccountGroup.getGroupName()).isEqualTo(DEFAULT_GROUP_NAME);
        assertThat(testAccountGroup.getDefaultActive()).isEqualTo(DEFAULT_DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    void createAccountGroupWithExistingId() throws Exception {
        // Create the AccountGroup with an existing ID
        accountGroup.setId(1L);
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);

        int databaseSizeBeforeCreate = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccountGroupMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeCreate);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
    }

    @Test
    @Transactional
    void checkGroupNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        // set the field null
        accountGroup.setGroupName(null);

        // Create the AccountGroup, which fails.
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);

        restAccountGroupMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isBadRequest());

        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeTest);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
    }

    @Test
    @Transactional
    void checkDefaultActiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        // set the field null
        accountGroup.setDefaultActive(null);

        // Create the AccountGroup, which fails.
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);

        restAccountGroupMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isBadRequest());

        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeTest);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
    }

    @Test
    @Transactional
    void getAllAccountGroups() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get all the accountGroupList
        restAccountGroupMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accountGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME)))
            .andExpect(jsonPath("$.[*].defaultActive").value(hasItem(DEFAULT_DEFAULT_ACTIVE.booleanValue())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllAccountGroupsWithEagerRelationshipsIsEnabled() throws Exception {
        when(accountGroupServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restAccountGroupMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(accountGroupServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllAccountGroupsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(accountGroupServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restAccountGroupMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(accountGroupRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getAccountGroup() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get the accountGroup
        restAccountGroupMockMvc
            .perform(get(ENTITY_API_URL_ID, accountGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(accountGroup.getId().intValue()))
            .andExpect(jsonPath("$.groupName").value(DEFAULT_GROUP_NAME))
            .andExpect(jsonPath("$.defaultActive").value(DEFAULT_DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    void getAccountGroupsByIdFiltering() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        Long id = accountGroup.getId();

        defaultAccountGroupShouldBeFound("id.equals=" + id);
        defaultAccountGroupShouldNotBeFound("id.notEquals=" + id);

        defaultAccountGroupShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAccountGroupShouldNotBeFound("id.greaterThan=" + id);

        defaultAccountGroupShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAccountGroupShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllAccountGroupsByGroupNameIsEqualToSomething() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get all the accountGroupList where groupName equals to DEFAULT_GROUP_NAME
        defaultAccountGroupShouldBeFound("groupName.equals=" + DEFAULT_GROUP_NAME);

        // Get all the accountGroupList where groupName equals to UPDATED_GROUP_NAME
        defaultAccountGroupShouldNotBeFound("groupName.equals=" + UPDATED_GROUP_NAME);
    }

    @Test
    @Transactional
    void getAllAccountGroupsByGroupNameIsInShouldWork() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get all the accountGroupList where groupName in DEFAULT_GROUP_NAME or UPDATED_GROUP_NAME
        defaultAccountGroupShouldBeFound("groupName.in=" + DEFAULT_GROUP_NAME + "," + UPDATED_GROUP_NAME);

        // Get all the accountGroupList where groupName equals to UPDATED_GROUP_NAME
        defaultAccountGroupShouldNotBeFound("groupName.in=" + UPDATED_GROUP_NAME);
    }

    @Test
    @Transactional
    void getAllAccountGroupsByGroupNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get all the accountGroupList where groupName is not null
        defaultAccountGroupShouldBeFound("groupName.specified=true");

        // Get all the accountGroupList where groupName is null
        defaultAccountGroupShouldNotBeFound("groupName.specified=false");
    }

    @Test
    @Transactional
    void getAllAccountGroupsByGroupNameContainsSomething() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get all the accountGroupList where groupName contains DEFAULT_GROUP_NAME
        defaultAccountGroupShouldBeFound("groupName.contains=" + DEFAULT_GROUP_NAME);

        // Get all the accountGroupList where groupName contains UPDATED_GROUP_NAME
        defaultAccountGroupShouldNotBeFound("groupName.contains=" + UPDATED_GROUP_NAME);
    }

    @Test
    @Transactional
    void getAllAccountGroupsByGroupNameNotContainsSomething() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get all the accountGroupList where groupName does not contain DEFAULT_GROUP_NAME
        defaultAccountGroupShouldNotBeFound("groupName.doesNotContain=" + DEFAULT_GROUP_NAME);

        // Get all the accountGroupList where groupName does not contain UPDATED_GROUP_NAME
        defaultAccountGroupShouldBeFound("groupName.doesNotContain=" + UPDATED_GROUP_NAME);
    }

    @Test
    @Transactional
    void getAllAccountGroupsByDefaultActiveIsEqualToSomething() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get all the accountGroupList where defaultActive equals to DEFAULT_DEFAULT_ACTIVE
        defaultAccountGroupShouldBeFound("defaultActive.equals=" + DEFAULT_DEFAULT_ACTIVE);

        // Get all the accountGroupList where defaultActive equals to UPDATED_DEFAULT_ACTIVE
        defaultAccountGroupShouldNotBeFound("defaultActive.equals=" + UPDATED_DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    void getAllAccountGroupsByDefaultActiveIsInShouldWork() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get all the accountGroupList where defaultActive in DEFAULT_DEFAULT_ACTIVE or UPDATED_DEFAULT_ACTIVE
        defaultAccountGroupShouldBeFound("defaultActive.in=" + DEFAULT_DEFAULT_ACTIVE + "," + UPDATED_DEFAULT_ACTIVE);

        // Get all the accountGroupList where defaultActive equals to UPDATED_DEFAULT_ACTIVE
        defaultAccountGroupShouldNotBeFound("defaultActive.in=" + UPDATED_DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    void getAllAccountGroupsByDefaultActiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        // Get all the accountGroupList where defaultActive is not null
        defaultAccountGroupShouldBeFound("defaultActive.specified=true");

        // Get all the accountGroupList where defaultActive is null
        defaultAccountGroupShouldNotBeFound("defaultActive.specified=false");
    }

    @Test
    @Transactional
    void getAllAccountGroupsByAccountIsEqualToSomething() throws Exception {
        ApplicationUser account;
        if (TestUtil.findAll(em, ApplicationUser.class).isEmpty()) {
            accountGroupRepository.saveAndFlush(accountGroup);
            account = ApplicationUserResourceIT.createEntity(em);
        } else {
            account = TestUtil.findAll(em, ApplicationUser.class).get(0);
        }
        em.persist(account);
        em.flush();
        accountGroup.setAccount(account);
        accountGroupRepository.saveAndFlush(accountGroup);
        Long accountId = account.getId();
        // Get all the accountGroupList where account equals to accountId
        defaultAccountGroupShouldBeFound("accountId.equals=" + accountId);

        // Get all the accountGroupList where account equals to (accountId + 1)
        defaultAccountGroupShouldNotBeFound("accountId.equals=" + (accountId + 1));
    }

    @Test
    @Transactional
    void getAllAccountGroupsByBankAccountIsEqualToSomething() throws Exception {
        BankAccount bankAccount;
        if (TestUtil.findAll(em, BankAccount.class).isEmpty()) {
            accountGroupRepository.saveAndFlush(accountGroup);
            bankAccount = BankAccountResourceIT.createEntity(em);
        } else {
            bankAccount = TestUtil.findAll(em, BankAccount.class).get(0);
        }
        em.persist(bankAccount);
        em.flush();
        accountGroup.addBankAccount(bankAccount);
        accountGroupRepository.saveAndFlush(accountGroup);
        Long bankAccountId = bankAccount.getId();
        // Get all the accountGroupList where bankAccount equals to bankAccountId
        defaultAccountGroupShouldBeFound("bankAccountId.equals=" + bankAccountId);

        // Get all the accountGroupList where bankAccount equals to (bankAccountId + 1)
        defaultAccountGroupShouldNotBeFound("bankAccountId.equals=" + (bankAccountId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAccountGroupShouldBeFound(String filter) throws Exception {
        restAccountGroupMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accountGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME)))
            .andExpect(jsonPath("$.[*].defaultActive").value(hasItem(DEFAULT_DEFAULT_ACTIVE.booleanValue())));

        // Check, that the count call also returns 1
        restAccountGroupMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAccountGroupShouldNotBeFound(String filter) throws Exception {
        restAccountGroupMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAccountGroupMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingAccountGroup() throws Exception {
        // Get the accountGroup
        restAccountGroupMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingAccountGroup() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        int databaseSizeBeforeUpdate = accountGroupRepository.findAll().size();
        accountGroupSearchRepository.save(accountGroup);
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());

        // Update the accountGroup
        AccountGroup updatedAccountGroup = accountGroupRepository.findById(accountGroup.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedAccountGroup are not directly saved in db
        em.detach(updatedAccountGroup);
        updatedAccountGroup.groupName(UPDATED_GROUP_NAME).defaultActive(UPDATED_DEFAULT_ACTIVE);
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(updatedAccountGroup);

        restAccountGroupMockMvc
            .perform(
                put(ENTITY_API_URL_ID, accountGroupDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isOk());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeUpdate);
        AccountGroup testAccountGroup = accountGroupList.get(accountGroupList.size() - 1);
        assertThat(testAccountGroup.getGroupName()).isEqualTo(UPDATED_GROUP_NAME);
        assertThat(testAccountGroup.getDefaultActive()).isEqualTo(UPDATED_DEFAULT_ACTIVE);
        await()
            .atMost(5, TimeUnit.SECONDS)
            .untilAsserted(() -> {
                int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
                assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
                List<AccountGroup> accountGroupSearchList = IterableUtils.toList(accountGroupSearchRepository.findAll());
                AccountGroup testAccountGroupSearch = accountGroupSearchList.get(searchDatabaseSizeAfter - 1);
                assertThat(testAccountGroupSearch.getGroupName()).isEqualTo(UPDATED_GROUP_NAME);
                assertThat(testAccountGroupSearch.getDefaultActive()).isEqualTo(UPDATED_DEFAULT_ACTIVE);
            });
    }

    @Test
    @Transactional
    void putNonExistingAccountGroup() throws Exception {
        int databaseSizeBeforeUpdate = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        accountGroup.setId(longCount.incrementAndGet());

        // Create the AccountGroup
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccountGroupMockMvc
            .perform(
                put(ENTITY_API_URL_ID, accountGroupDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeUpdate);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
    }

    @Test
    @Transactional
    void putWithIdMismatchAccountGroup() throws Exception {
        int databaseSizeBeforeUpdate = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        accountGroup.setId(longCount.incrementAndGet());

        // Create the AccountGroup
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountGroupMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeUpdate);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAccountGroup() throws Exception {
        int databaseSizeBeforeUpdate = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        accountGroup.setId(longCount.incrementAndGet());

        // Create the AccountGroup
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountGroupMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeUpdate);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
    }

    @Test
    @Transactional
    void partialUpdateAccountGroupWithPatch() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        int databaseSizeBeforeUpdate = accountGroupRepository.findAll().size();

        // Update the accountGroup using partial update
        AccountGroup partialUpdatedAccountGroup = new AccountGroup();
        partialUpdatedAccountGroup.setId(accountGroup.getId());

        restAccountGroupMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAccountGroup.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAccountGroup))
            )
            .andExpect(status().isOk());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeUpdate);
        AccountGroup testAccountGroup = accountGroupList.get(accountGroupList.size() - 1);
        assertThat(testAccountGroup.getGroupName()).isEqualTo(DEFAULT_GROUP_NAME);
        assertThat(testAccountGroup.getDefaultActive()).isEqualTo(DEFAULT_DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    void fullUpdateAccountGroupWithPatch() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);

        int databaseSizeBeforeUpdate = accountGroupRepository.findAll().size();

        // Update the accountGroup using partial update
        AccountGroup partialUpdatedAccountGroup = new AccountGroup();
        partialUpdatedAccountGroup.setId(accountGroup.getId());

        partialUpdatedAccountGroup.groupName(UPDATED_GROUP_NAME).defaultActive(UPDATED_DEFAULT_ACTIVE);

        restAccountGroupMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAccountGroup.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAccountGroup))
            )
            .andExpect(status().isOk());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeUpdate);
        AccountGroup testAccountGroup = accountGroupList.get(accountGroupList.size() - 1);
        assertThat(testAccountGroup.getGroupName()).isEqualTo(UPDATED_GROUP_NAME);
        assertThat(testAccountGroup.getDefaultActive()).isEqualTo(UPDATED_DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    void patchNonExistingAccountGroup() throws Exception {
        int databaseSizeBeforeUpdate = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        accountGroup.setId(longCount.incrementAndGet());

        // Create the AccountGroup
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccountGroupMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, accountGroupDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeUpdate);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAccountGroup() throws Exception {
        int databaseSizeBeforeUpdate = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        accountGroup.setId(longCount.incrementAndGet());

        // Create the AccountGroup
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountGroupMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeUpdate);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAccountGroup() throws Exception {
        int databaseSizeBeforeUpdate = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        accountGroup.setId(longCount.incrementAndGet());

        // Create the AccountGroup
        AccountGroupDTO accountGroupDTO = accountGroupMapper.toDto(accountGroup);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountGroupMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(accountGroupDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AccountGroup in the database
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeUpdate);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore);
    }

    @Test
    @Transactional
    void deleteAccountGroup() throws Exception {
        // Initialize the database
        accountGroupRepository.saveAndFlush(accountGroup);
        accountGroupRepository.save(accountGroup);
        accountGroupSearchRepository.save(accountGroup);

        int databaseSizeBeforeDelete = accountGroupRepository.findAll().size();
        int searchDatabaseSizeBefore = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeBefore).isEqualTo(databaseSizeBeforeDelete);

        // Delete the accountGroup
        restAccountGroupMockMvc
            .perform(delete(ENTITY_API_URL_ID, accountGroup.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AccountGroup> accountGroupList = accountGroupRepository.findAll();
        assertThat(accountGroupList).hasSize(databaseSizeBeforeDelete - 1);
        int searchDatabaseSizeAfter = IterableUtil.sizeOf(accountGroupSearchRepository.findAll());
        assertThat(searchDatabaseSizeAfter).isEqualTo(searchDatabaseSizeBefore - 1);
    }

    @Test
    @Transactional
    void searchAccountGroup() throws Exception {
        // Initialize the database
        accountGroup = accountGroupRepository.saveAndFlush(accountGroup);
        accountGroupSearchRepository.save(accountGroup);

        // Search the accountGroup
        restAccountGroupMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + accountGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accountGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME)))
            .andExpect(jsonPath("$.[*].defaultActive").value(hasItem(DEFAULT_DEFAULT_ACTIVE.booleanValue())));
    }
}
