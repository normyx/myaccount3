import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('AccountGroup e2e test', () => {
  const accountGroupPageUrl = '/account-group';
  const accountGroupPageUrlPattern = new RegExp('/account-group(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  // const accountGroupSample = {"groupName":"super","defaultActive":false};

  let accountGroup;
  // let applicationUser;

  beforeEach(() => {
    cy.login(username, password);
  });

  /* Disabled due to incompatibility
  beforeEach(() => {
    // create an instance at the required relationship entity:
    cy.authenticatedRequest({
      method: 'POST',
      url: '/api/application-users',
      body: {"nickName":"exiger"},
    }).then(({ body }) => {
      applicationUser = body;
    });
  });
   */

  beforeEach(() => {
    cy.intercept('GET', '/api/account-groups+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/account-groups').as('postEntityRequest');
    cy.intercept('DELETE', '/api/account-groups/*').as('deleteEntityRequest');
  });

  /* Disabled due to incompatibility
  beforeEach(() => {
    // Simulate relationships api for better performance and reproducibility.
    cy.intercept('GET', '/api/application-users', {
      statusCode: 200,
      body: [applicationUser],
    });

    cy.intercept('GET', '/api/bank-accounts', {
      statusCode: 200,
      body: [],
    });

  });
   */

  afterEach(() => {
    if (accountGroup) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/account-groups/${accountGroup.id}`,
      }).then(() => {
        accountGroup = undefined;
      });
    }
  });

  /* Disabled due to incompatibility
  afterEach(() => {
    if (applicationUser) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/application-users/${applicationUser.id}`,
      }).then(() => {
        applicationUser = undefined;
      });
    }
  });
   */

  it('AccountGroups menu should load AccountGroups page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('account-group');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('AccountGroup').should('exist');
    cy.url().should('match', accountGroupPageUrlPattern);
  });

  describe('AccountGroup page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(accountGroupPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create AccountGroup page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/account-group/new$'));
        cy.getEntityCreateUpdateHeading('AccountGroup');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', accountGroupPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      /* Disabled due to incompatibility
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/account-groups',
          body: {
            ...accountGroupSample,
            account: applicationUser,
          },
        }).then(({ body }) => {
          accountGroup = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/account-groups+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/account-groups?page=0&size=20>; rel="last",<http://localhost/api/account-groups?page=0&size=20>; rel="first"',
              },
              body: [accountGroup],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(accountGroupPageUrl);

        cy.wait('@entitiesRequestInternal');
      });
       */

      beforeEach(function () {
        cy.visit(accountGroupPageUrl);

        cy.wait('@entitiesRequest').then(({ response }) => {
          if (response.body.length === 0) {
            this.skip();
          }
        });
      });

      it('detail button click should load details AccountGroup page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('accountGroup');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', accountGroupPageUrlPattern);
      });

      it('edit button click should load edit AccountGroup page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('AccountGroup');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', accountGroupPageUrlPattern);
      });

      it.skip('edit button click should load edit AccountGroup page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('AccountGroup');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', accountGroupPageUrlPattern);
      });

      it.skip('last delete button click should delete instance of AccountGroup', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('accountGroup').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', accountGroupPageUrlPattern);

        accountGroup = undefined;
      });
    });
  });

  describe('new AccountGroup page', () => {
    beforeEach(() => {
      cy.visit(`${accountGroupPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('AccountGroup');
    });

    it.skip('should create an instance of AccountGroup', () => {
      cy.get(`[data-cy="groupName"]`).type('avant que commissionnaire');
      cy.get(`[data-cy="groupName"]`).should('have.value', 'avant que commissionnaire');

      cy.get(`[data-cy="defaultActive"]`).should('not.be.checked');
      cy.get(`[data-cy="defaultActive"]`).click();
      cy.get(`[data-cy="defaultActive"]`).should('be.checked');

      cy.get(`[data-cy="account"]`).select(1);

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        accountGroup = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', accountGroupPageUrlPattern);
    });
  });
});
