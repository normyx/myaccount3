import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('StockPortfolioItem e2e test', () => {
  const stockPortfolioItemPageUrl = '/stock-portfolio-item';
  const stockPortfolioItemPageUrlPattern = new RegExp('/stock-portfolio-item(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const stockPortfolioItemSample = {
    stockSymbol: 'lunatique ',
    stockCurrency: 'GBP',
    stockAcquisitionDate: '2024-03-03',
    stockSharesNumber: 21137.7,
    stockAcquisitionPrice: 12855.06,
    stockCurrentPrice: 2488.44,
    stockCurrentDate: '2024-03-03',
    stockAcquisitionCurrencyFactor: 1719.3,
    stockCurrentCurrencyFactor: 4079.05,
    stockPriceAtAcquisitionDate: 32662.31,
    stockType: 'STOCK',
  };

  let stockPortfolioItem;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/stock-portfolio-items+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/stock-portfolio-items').as('postEntityRequest');
    cy.intercept('DELETE', '/api/stock-portfolio-items/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (stockPortfolioItem) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/stock-portfolio-items/${stockPortfolioItem.id}`,
      }).then(() => {
        stockPortfolioItem = undefined;
      });
    }
  });

  it('StockPortfolioItems menu should load StockPortfolioItems page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('stock-portfolio-item');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('StockPortfolioItem').should('exist');
    cy.url().should('match', stockPortfolioItemPageUrlPattern);
  });

  describe('StockPortfolioItem page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(stockPortfolioItemPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create StockPortfolioItem page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/stock-portfolio-item/new$'));
        cy.getEntityCreateUpdateHeading('StockPortfolioItem');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', stockPortfolioItemPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/stock-portfolio-items',
          body: stockPortfolioItemSample,
        }).then(({ body }) => {
          stockPortfolioItem = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/stock-portfolio-items+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/stock-portfolio-items?page=0&size=20>; rel="last",<http://localhost/api/stock-portfolio-items?page=0&size=20>; rel="first"',
              },
              body: [stockPortfolioItem],
            },
          ).as('entitiesRequestInternal');
        });

        cy.visit(stockPortfolioItemPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details StockPortfolioItem page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('stockPortfolioItem');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', stockPortfolioItemPageUrlPattern);
      });

      it('edit button click should load edit StockPortfolioItem page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('StockPortfolioItem');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', stockPortfolioItemPageUrlPattern);
      });

      it.skip('edit button click should load edit StockPortfolioItem page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('StockPortfolioItem');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', stockPortfolioItemPageUrlPattern);
      });

      it('last delete button click should delete instance of StockPortfolioItem', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('stockPortfolioItem').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', stockPortfolioItemPageUrlPattern);

        stockPortfolioItem = undefined;
      });
    });
  });

  describe('new StockPortfolioItem page', () => {
    beforeEach(() => {
      cy.visit(`${stockPortfolioItemPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('StockPortfolioItem');
    });

    it('should create an instance of StockPortfolioItem', () => {
      cy.get(`[data-cy="stockSymbol"]`).type('grandement');
      cy.get(`[data-cy="stockSymbol"]`).should('have.value', 'grandement');

      cy.get(`[data-cy="stockCurrency"]`).select('GBP');

      cy.get(`[data-cy="stockAcquisitionDate"]`).type('2024-03-03');
      cy.get(`[data-cy="stockAcquisitionDate"]`).blur();
      cy.get(`[data-cy="stockAcquisitionDate"]`).should('have.value', '2024-03-03');

      cy.get(`[data-cy="stockSharesNumber"]`).type('11098.83');
      cy.get(`[data-cy="stockSharesNumber"]`).should('have.value', '11098.83');

      cy.get(`[data-cy="stockAcquisitionPrice"]`).type('11235.53');
      cy.get(`[data-cy="stockAcquisitionPrice"]`).should('have.value', '11235.53');

      cy.get(`[data-cy="stockCurrentPrice"]`).type('4035.36');
      cy.get(`[data-cy="stockCurrentPrice"]`).should('have.value', '4035.36');

      cy.get(`[data-cy="stockCurrentDate"]`).type('2024-03-02');
      cy.get(`[data-cy="stockCurrentDate"]`).blur();
      cy.get(`[data-cy="stockCurrentDate"]`).should('have.value', '2024-03-02');

      cy.get(`[data-cy="stockAcquisitionCurrencyFactor"]`).type('11398.51');
      cy.get(`[data-cy="stockAcquisitionCurrencyFactor"]`).should('have.value', '11398.51');

      cy.get(`[data-cy="stockCurrentCurrencyFactor"]`).type('31510.5');
      cy.get(`[data-cy="stockCurrentCurrencyFactor"]`).should('have.value', '31510.5');

      cy.get(`[data-cy="stockPriceAtAcquisitionDate"]`).type('16397.13');
      cy.get(`[data-cy="stockPriceAtAcquisitionDate"]`).should('have.value', '16397.13');

      cy.get(`[data-cy="stockType"]`).select('CRYPTO');

      cy.get(`[data-cy="lastStockUpdate"]`).type('2024-03-02T19:41');
      cy.get(`[data-cy="lastStockUpdate"]`).blur();
      cy.get(`[data-cy="lastStockUpdate"]`).should('have.value', '2024-03-02T19:41');

      cy.get(`[data-cy="lastCurrencyUpdate"]`).type('2024-03-02T21:30');
      cy.get(`[data-cy="lastCurrencyUpdate"]`).blur();
      cy.get(`[data-cy="lastCurrencyUpdate"]`).should('have.value', '2024-03-02T21:30');

      cy.get(`[data-cy="stockSellDate"]`).type('2024-03-03');
      cy.get(`[data-cy="stockSellDate"]`).blur();
      cy.get(`[data-cy="stockSellDate"]`).should('have.value', '2024-03-03');

      cy.get(`[data-cy="stockSellPrice"]`).type('9874.2');
      cy.get(`[data-cy="stockSellPrice"]`).should('have.value', '9874.2');

      cy.get(`[data-cy="stockSellCurrencyFactor"]`).type('77.04');
      cy.get(`[data-cy="stockSellCurrencyFactor"]`).should('have.value', '77.04');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        stockPortfolioItem = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', stockPortfolioItemPageUrlPattern);
    });
  });
});
