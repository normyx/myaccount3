import { IApplicationUser, NewApplicationUser } from './application-user.model';

export const sampleWithRequiredData: IApplicationUser = {
  id: 30101,
  nickName: 'smack jeune enfant à côté de',
};

export const sampleWithPartialData: IApplicationUser = {
  id: 21584,
  nickName: 'administration selon sans que',
};

export const sampleWithFullData: IApplicationUser = {
  id: 10031,
  nickName: 'auprès de tout à fait',
};

export const sampleWithNewData: NewApplicationUser = {
  nickName: 'fonctionnaire charitable',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
