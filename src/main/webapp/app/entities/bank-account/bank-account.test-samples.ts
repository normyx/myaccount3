import { IBankAccount, NewBankAccount } from './bank-account.model';

export const sampleWithRequiredData: IBankAccount = {
  id: 26439,
  accountName: 'Chèque Account',
  accountBank: 'dense bof regrouper',
  initialAmount: 31661.98,
  archived: true,
  accountType: 'CURRENTACCOUNT',
};

export const sampleWithPartialData: IBankAccount = {
  id: 13004,
  accountName: 'Chèque Account',
  accountBank: 'cuicui bâtir de sorte que',
  initialAmount: 18770.2,
  archived: false,
  shortName: 'de peur que',
  accountType: 'CURRENTACCOUNT',
};

export const sampleWithFullData: IBankAccount = {
  id: 3715,
  accountName: 'Epargne Account',
  accountBank: 'retenir âcre même si',
  initialAmount: 161.27,
  archived: false,
  shortName: 'si',
  accountType: 'SAVINGSACCOUNT',
};

export const sampleWithNewData: NewBankAccount = {
  accountName: 'Epargne Account',
  accountBank: 'grandement en bas de',
  initialAmount: 22853.38,
  archived: true,
  accountType: 'STOCKPORTFOLIO',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
