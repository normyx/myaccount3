import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBudgetItemPeriod } from '../budget-item-period.model';
import { BudgetItemPeriodService } from '../service/budget-item-period.service';

export const budgetItemPeriodResolve = (route: ActivatedRouteSnapshot): Observable<null | IBudgetItemPeriod> => {
  const id = route.params['id'];
  if (id) {
    return inject(BudgetItemPeriodService)
      .find(id)
      .pipe(
        mergeMap((budgetItemPeriod: HttpResponse<IBudgetItemPeriod>) => {
          if (budgetItemPeriod.body) {
            return of(budgetItemPeriod.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default budgetItemPeriodResolve;
