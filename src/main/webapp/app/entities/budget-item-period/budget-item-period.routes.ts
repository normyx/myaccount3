import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { BudgetItemPeriodComponent } from './list/budget-item-period.component';
import { BudgetItemPeriodDetailComponent } from './detail/budget-item-period-detail.component';
import { BudgetItemPeriodUpdateComponent } from './update/budget-item-period-update.component';
import BudgetItemPeriodResolve from './route/budget-item-period-routing-resolve.service';

const budgetItemPeriodRoute: Routes = [
  {
    path: '',
    component: BudgetItemPeriodComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BudgetItemPeriodDetailComponent,
    resolve: {
      budgetItemPeriod: BudgetItemPeriodResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BudgetItemPeriodUpdateComponent,
    resolve: {
      budgetItemPeriod: BudgetItemPeriodResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BudgetItemPeriodUpdateComponent,
    resolve: {
      budgetItemPeriod: BudgetItemPeriodResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default budgetItemPeriodRoute;
