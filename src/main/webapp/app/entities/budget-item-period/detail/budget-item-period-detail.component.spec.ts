import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { BudgetItemPeriodDetailComponent } from './budget-item-period-detail.component';

describe('BudgetItemPeriod Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BudgetItemPeriodDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: BudgetItemPeriodDetailComponent,
              resolve: { budgetItemPeriod: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(BudgetItemPeriodDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load budgetItemPeriod on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', BudgetItemPeriodDetailComponent);

      // THEN
      expect(instance.budgetItemPeriod).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
