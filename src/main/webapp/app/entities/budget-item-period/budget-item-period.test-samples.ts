import dayjs from 'dayjs/esm';

import { IBudgetItemPeriod, NewBudgetItemPeriod } from './budget-item-period.model';

export const sampleWithRequiredData: IBudgetItemPeriod = {
  id: 14272,
  month: dayjs('2024-03-02'),
  amount: 32752.79,
};

export const sampleWithPartialData: IBudgetItemPeriod = {
  id: 18882,
  date: dayjs('2024-03-03'),
  month: dayjs('2024-03-03'),
  amount: 10113.44,
  isSmoothed: true,
};

export const sampleWithFullData: IBudgetItemPeriod = {
  id: 5792,
  date: dayjs('2024-03-03'),
  month: dayjs('2024-03-03'),
  amount: 7850.57,
  isSmoothed: true,
  isRecurrent: false,
};

export const sampleWithNewData: NewBudgetItemPeriod = {
  month: dayjs('2024-03-03'),
  amount: 30824.7,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
