import dayjs from 'dayjs/esm';

import { IOperation, NewOperation } from './operation.model';

export const sampleWithRequiredData: IOperation = {
  id: 22451,
  label: 'rectorat',
  date: dayjs('2024-03-02'),
  amount: 22623.15,
  isUpToDate: false,
};

export const sampleWithPartialData: IOperation = {
  id: 30885,
  label: 'hors de souffler',
  date: dayjs('2024-03-03'),
  amount: 13736.78,
  note: 'quoique boum',
  isUpToDate: false,
  deletingHardLock: true,
};

export const sampleWithFullData: IOperation = {
  id: 14471,
  label: 'candide ha chef de cuisine',
  date: dayjs('2024-03-02'),
  amount: 21833.87,
  note: 'hi lorsque',
  checkNumber: 'via badaboum',
  isUpToDate: true,
  deletingHardLock: true,
};

export const sampleWithNewData: NewOperation = {
  label: 'que',
  date: dayjs('2024-03-03'),
  amount: 16800.51,
  isUpToDate: true,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
