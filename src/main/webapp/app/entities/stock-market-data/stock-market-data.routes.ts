import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { StockMarketDataComponent } from './list/stock-market-data.component';
import { StockMarketDataDetailComponent } from './detail/stock-market-data-detail.component';
import { StockMarketDataUpdateComponent } from './update/stock-market-data-update.component';
import StockMarketDataResolve from './route/stock-market-data-routing-resolve.service';

const stockMarketDataRoute: Routes = [
  {
    path: '',
    component: StockMarketDataComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: StockMarketDataDetailComponent,
    resolve: {
      stockMarketData: StockMarketDataResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: StockMarketDataUpdateComponent,
    resolve: {
      stockMarketData: StockMarketDataResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: StockMarketDataUpdateComponent,
    resolve: {
      stockMarketData: StockMarketDataResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default stockMarketDataRoute;
