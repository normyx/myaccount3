import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { StockMarketDataDetailComponent } from './stock-market-data-detail.component';

describe('StockMarketData Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StockMarketDataDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: StockMarketDataDetailComponent,
              resolve: { stockMarketData: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(StockMarketDataDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load stockMarketData on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', StockMarketDataDetailComponent);

      // THEN
      expect(instance.stockMarketData).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
