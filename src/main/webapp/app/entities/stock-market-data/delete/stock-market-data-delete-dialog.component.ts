import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IStockMarketData } from '../stock-market-data.model';
import { StockMarketDataService } from '../service/stock-market-data.service';

@Component({
  standalone: true,
  templateUrl: './stock-market-data-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class StockMarketDataDeleteDialogComponent {
  stockMarketData?: IStockMarketData;

  constructor(
    protected stockMarketDataService: StockMarketDataService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.stockMarketDataService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
