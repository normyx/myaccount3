import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IStockMarketData } from '../stock-market-data.model';
import { StockMarketDataService } from '../service/stock-market-data.service';

export const stockMarketDataResolve = (route: ActivatedRouteSnapshot): Observable<null | IStockMarketData> => {
  const id = route.params['id'];
  if (id) {
    return inject(StockMarketDataService)
      .find(id)
      .pipe(
        mergeMap((stockMarketData: HttpResponse<IStockMarketData>) => {
          if (stockMarketData.body) {
            return of(stockMarketData.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default stockMarketDataResolve;
