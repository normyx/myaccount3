import dayjs from 'dayjs/esm';

import { IStockMarketData, NewStockMarketData } from './stock-market-data.model';

export const sampleWithRequiredData: IStockMarketData = {
  id: 24365,
  symbol: 'collègue c',
  dataDate: dayjs('2024-03-03'),
  closeValue: 19353.6,
};

export const sampleWithPartialData: IStockMarketData = {
  id: 9543,
  symbol: 'chef de cu',
  dataDate: dayjs('2024-03-03'),
  closeValue: 31098.96,
};

export const sampleWithFullData: IStockMarketData = {
  id: 17613,
  symbol: 'pin-pon',
  dataDate: dayjs('2024-03-03'),
  closeValue: 24960.5,
};

export const sampleWithNewData: NewStockMarketData = {
  symbol: 'diplomate ',
  dataDate: dayjs('2024-03-03'),
  closeValue: 15439.46,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
