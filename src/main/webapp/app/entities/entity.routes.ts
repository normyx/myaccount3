import { Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'account-group',
    data: { pageTitle: 'myaccount3App.accountGroup.home.title' },
    loadChildren: () => import('./account-group/account-group.routes'),
  },
  {
    path: 'application-user',
    data: { pageTitle: 'myaccount3App.applicationUser.home.title' },
    loadChildren: () => import('./application-user/application-user.routes'),
  },
  {
    path: 'bank-account',
    data: { pageTitle: 'myaccount3App.bankAccount.home.title' },
    loadChildren: () => import('./bank-account/bank-account.routes'),
  },
  {
    path: 'budget-item',
    data: { pageTitle: 'myaccount3App.budgetItem.home.title' },
    loadChildren: () => import('./budget-item/budget-item.routes'),
  },
  {
    path: 'budget-item-period',
    data: { pageTitle: 'myaccount3App.budgetItemPeriod.home.title' },
    loadChildren: () => import('./budget-item-period/budget-item-period.routes'),
  },
  {
    path: 'category',
    data: { pageTitle: 'myaccount3App.category.home.title' },
    loadChildren: () => import('./category/category.routes'),
  },
  {
    path: 'operation',
    data: { pageTitle: 'myaccount3App.operation.home.title' },
    loadChildren: () => import('./operation/operation.routes'),
  },
  {
    path: 'real-estate-item',
    data: { pageTitle: 'myaccount3App.realEstateItem.home.title' },
    loadChildren: () => import('./real-estate-item/real-estate-item.routes'),
  },
  {
    path: 'stock-market-data',
    data: { pageTitle: 'myaccount3App.stockMarketData.home.title' },
    loadChildren: () => import('./stock-market-data/stock-market-data.routes'),
  },
  {
    path: 'stock-portfolio-item',
    data: { pageTitle: 'myaccount3App.stockPortfolioItem.home.title' },
    loadChildren: () => import('./stock-portfolio-item/stock-portfolio-item.routes'),
  },
  {
    path: 'sub-category',
    data: { pageTitle: 'myaccount3App.subCategory.home.title' },
    loadChildren: () => import('./sub-category/sub-category.routes'),
  },
  /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
];

export default routes;
