import { ISubCategory, NewSubCategory } from './sub-category.model';

export const sampleWithRequiredData: ISubCategory = {
  id: 14092,
  subCategoryName: 'zzzz',
};

export const sampleWithPartialData: ISubCategory = {
  id: 22794,
  subCategoryName: 'chef',
};

export const sampleWithFullData: ISubCategory = {
  id: 26944,
  subCategoryName: 'infiniment',
};

export const sampleWithNewData: NewSubCategory = {
  subCategoryName: 'incalculable entre détacher',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
