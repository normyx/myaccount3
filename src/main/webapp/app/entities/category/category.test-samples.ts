import { ICategory, NewCategory } from './category.model';

export const sampleWithRequiredData: ICategory = {
  id: 17801,
  categoryName: 'secours adepte proclamer',
  categoryType: 'OTHER',
};

export const sampleWithPartialData: ICategory = {
  id: 3245,
  categoryName: 'tsoin-tsoin',
  categoryType: 'REVENUE',
};

export const sampleWithFullData: ICategory = {
  id: 20643,
  categoryName: 'jeune enfant',
  categoryType: 'SPENDING',
};

export const sampleWithNewData: NewCategory = {
  categoryName: 'rapide',
  categoryType: 'REVENUE',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
