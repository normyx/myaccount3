import { ISubCategory } from 'app/entities/sub-category/sub-category.model';
import { CategoryType } from 'app/entities/enumerations/category-type.model';

export interface ICategory {
  id: number;
  categoryName?: string | null;
  categoryType?: keyof typeof CategoryType | null;
  subCategories?: Pick<ISubCategory, 'id'>[] | null;
}

export type NewCategory = Omit<ICategory, 'id'> & { id: null };
