import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { StockPortfolioItemDetailComponent } from './stock-portfolio-item-detail.component';

describe('StockPortfolioItem Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StockPortfolioItemDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: StockPortfolioItemDetailComponent,
              resolve: { stockPortfolioItem: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(StockPortfolioItemDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load stockPortfolioItem on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', StockPortfolioItemDetailComponent);

      // THEN
      expect(instance.stockPortfolioItem).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
