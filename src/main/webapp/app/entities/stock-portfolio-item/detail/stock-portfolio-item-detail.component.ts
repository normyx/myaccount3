import { Component, Input } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IStockPortfolioItem } from '../stock-portfolio-item.model';

@Component({
  standalone: true,
  selector: 'jhi-stock-portfolio-item-detail',
  templateUrl: './stock-portfolio-item-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class StockPortfolioItemDetailComponent {
  @Input() stockPortfolioItem: IStockPortfolioItem | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  previousState(): void {
    window.history.back();
  }
}
