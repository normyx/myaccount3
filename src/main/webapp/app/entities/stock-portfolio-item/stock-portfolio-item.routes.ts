import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { StockPortfolioItemComponent } from './list/stock-portfolio-item.component';
import { StockPortfolioItemDetailComponent } from './detail/stock-portfolio-item-detail.component';
import { StockPortfolioItemUpdateComponent } from './update/stock-portfolio-item-update.component';
import StockPortfolioItemResolve from './route/stock-portfolio-item-routing-resolve.service';

const stockPortfolioItemRoute: Routes = [
  {
    path: '',
    component: StockPortfolioItemComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: StockPortfolioItemDetailComponent,
    resolve: {
      stockPortfolioItem: StockPortfolioItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: StockPortfolioItemUpdateComponent,
    resolve: {
      stockPortfolioItem: StockPortfolioItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: StockPortfolioItemUpdateComponent,
    resolve: {
      stockPortfolioItem: StockPortfolioItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default stockPortfolioItemRoute;
