import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IStockPortfolioItem } from '../stock-portfolio-item.model';
import { StockPortfolioItemService } from '../service/stock-portfolio-item.service';

export const stockPortfolioItemResolve = (route: ActivatedRouteSnapshot): Observable<null | IStockPortfolioItem> => {
  const id = route.params['id'];
  if (id) {
    return inject(StockPortfolioItemService)
      .find(id)
      .pipe(
        mergeMap((stockPortfolioItem: HttpResponse<IStockPortfolioItem>) => {
          if (stockPortfolioItem.body) {
            return of(stockPortfolioItem.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default stockPortfolioItemResolve;
