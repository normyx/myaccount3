import dayjs from 'dayjs/esm';

import { IStockPortfolioItem, NewStockPortfolioItem } from './stock-portfolio-item.model';

export const sampleWithRequiredData: IStockPortfolioItem = {
  id: 8651,
  stockSymbol: 'après parf',
  stockCurrency: 'USD',
  stockAcquisitionDate: dayjs('2024-03-03'),
  stockSharesNumber: 14055.66,
  stockAcquisitionPrice: 6719.24,
  stockCurrentPrice: 6647.62,
  stockCurrentDate: dayjs('2024-03-03'),
  stockAcquisitionCurrencyFactor: 25118.25,
  stockCurrentCurrencyFactor: 24951.27,
  stockPriceAtAcquisitionDate: 29423.77,
  stockType: 'CRYPTO',
};

export const sampleWithPartialData: IStockPortfolioItem = {
  id: 13617,
  stockSymbol: 'patientèle',
  stockCurrency: 'EUR',
  stockAcquisitionDate: dayjs('2024-03-02'),
  stockSharesNumber: 29532.2,
  stockAcquisitionPrice: 28913.37,
  stockCurrentPrice: 12469.31,
  stockCurrentDate: dayjs('2024-03-03'),
  stockAcquisitionCurrencyFactor: 26929.57,
  stockCurrentCurrencyFactor: 26994.19,
  stockPriceAtAcquisitionDate: 25773.99,
  stockType: 'STOCK',
  lastStockUpdate: dayjs('2024-03-02T17:20'),
  stockSellPrice: 23949.1,
  stockSellCurrencyFactor: 28075.78,
};

export const sampleWithFullData: IStockPortfolioItem = {
  id: 31099,
  stockSymbol: 'mélancoliq',
  stockCurrency: 'USD',
  stockAcquisitionDate: dayjs('2024-03-03'),
  stockSharesNumber: 17750.02,
  stockAcquisitionPrice: 2233.28,
  stockCurrentPrice: 12358.7,
  stockCurrentDate: dayjs('2024-03-02'),
  stockAcquisitionCurrencyFactor: 31653.81,
  stockCurrentCurrencyFactor: 5880.7,
  stockPriceAtAcquisitionDate: 25941.51,
  stockType: 'CRYPTO',
  lastStockUpdate: dayjs('2024-03-03T02:32'),
  lastCurrencyUpdate: dayjs('2024-03-03T00:22'),
  stockSellDate: dayjs('2024-03-03'),
  stockSellPrice: 8276.57,
  stockSellCurrencyFactor: 32495.98,
};

export const sampleWithNewData: NewStockPortfolioItem = {
  stockSymbol: 'pin-pon bi',
  stockCurrency: 'USD',
  stockAcquisitionDate: dayjs('2024-03-02'),
  stockSharesNumber: 27648.18,
  stockAcquisitionPrice: 1532.72,
  stockCurrentPrice: 4552.2,
  stockCurrentDate: dayjs('2024-03-02'),
  stockAcquisitionCurrencyFactor: 6168.21,
  stockCurrentCurrencyFactor: 13708,
  stockPriceAtAcquisitionDate: 1362.76,
  stockType: 'STOCK',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
