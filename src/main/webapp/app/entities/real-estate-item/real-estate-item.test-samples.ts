import dayjs from 'dayjs/esm';

import { IRealEstateItem, NewRealEstateItem } from './real-estate-item.model';

export const sampleWithRequiredData: IRealEstateItem = {
  id: 17287,
  loanValue: 24992.15,
  totalValue: 16459.02,
  percentOwned: 56.75,
  itemDate: dayjs('2024-03-03'),
};

export const sampleWithPartialData: IRealEstateItem = {
  id: 23813,
  loanValue: 1754.93,
  totalValue: 15958.81,
  percentOwned: 96.41,
  itemDate: dayjs('2024-03-03'),
};

export const sampleWithFullData: IRealEstateItem = {
  id: 15110,
  loanValue: 12540.66,
  totalValue: 24562.94,
  percentOwned: 60.43,
  itemDate: dayjs('2024-03-03'),
};

export const sampleWithNewData: NewRealEstateItem = {
  loanValue: 21659.19,
  totalValue: 9530.71,
  percentOwned: 45.3,
  itemDate: dayjs('2024-03-02'),
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
