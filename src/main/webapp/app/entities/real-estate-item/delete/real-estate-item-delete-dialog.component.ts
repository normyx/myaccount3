import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IRealEstateItem } from '../real-estate-item.model';
import { RealEstateItemService } from '../service/real-estate-item.service';

@Component({
  standalone: true,
  templateUrl: './real-estate-item-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class RealEstateItemDeleteDialogComponent {
  realEstateItem?: IRealEstateItem;

  constructor(
    protected realEstateItemService: RealEstateItemService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.realEstateItemService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
