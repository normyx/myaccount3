import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { RealEstateItemComponent } from './list/real-estate-item.component';
import { RealEstateItemDetailComponent } from './detail/real-estate-item-detail.component';
import { RealEstateItemUpdateComponent } from './update/real-estate-item-update.component';
import RealEstateItemResolve from './route/real-estate-item-routing-resolve.service';

const realEstateItemRoute: Routes = [
  {
    path: '',
    component: RealEstateItemComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RealEstateItemDetailComponent,
    resolve: {
      realEstateItem: RealEstateItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RealEstateItemUpdateComponent,
    resolve: {
      realEstateItem: RealEstateItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RealEstateItemUpdateComponent,
    resolve: {
      realEstateItem: RealEstateItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default realEstateItemRoute;
