import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { RealEstateItemDetailComponent } from './real-estate-item-detail.component';

describe('RealEstateItem Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RealEstateItemDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: RealEstateItemDetailComponent,
              resolve: { realEstateItem: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(RealEstateItemDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load realEstateItem on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', RealEstateItemDetailComponent);

      // THEN
      expect(instance.realEstateItem).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
