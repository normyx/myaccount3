import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IRealEstateItem } from '../real-estate-item.model';
import { RealEstateItemService } from '../service/real-estate-item.service';

export const realEstateItemResolve = (route: ActivatedRouteSnapshot): Observable<null | IRealEstateItem> => {
  const id = route.params['id'];
  if (id) {
    return inject(RealEstateItemService)
      .find(id)
      .pipe(
        mergeMap((realEstateItem: HttpResponse<IRealEstateItem>) => {
          if (realEstateItem.body) {
            return of(realEstateItem.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default realEstateItemResolve;
