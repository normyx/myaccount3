import { IBudgetItemPeriod } from 'app/entities/budget-item-period/budget-item-period.model';
import { ICategory } from 'app/entities/category/category.model';
import { IApplicationUser } from 'app/entities/application-user/application-user.model';

export interface IBudgetItem {
  id: number;
  name?: string | null;
  order?: number | null;
  budgetItemPeriods?: Pick<IBudgetItemPeriod, 'id'>[] | null;
  category?: Pick<ICategory, 'id' | 'categoryName'> | null;
  account?: Pick<IApplicationUser, 'id' | 'nickName'> | null;
}

export type NewBudgetItem = Omit<IBudgetItem, 'id'> & { id: null };
