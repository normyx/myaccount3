import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { BudgetItemComponent } from './list/budget-item.component';
import { BudgetItemDetailComponent } from './detail/budget-item-detail.component';
import { BudgetItemUpdateComponent } from './update/budget-item-update.component';
import BudgetItemResolve from './route/budget-item-routing-resolve.service';

const budgetItemRoute: Routes = [
  {
    path: '',
    component: BudgetItemComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BudgetItemDetailComponent,
    resolve: {
      budgetItem: BudgetItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BudgetItemUpdateComponent,
    resolve: {
      budgetItem: BudgetItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BudgetItemUpdateComponent,
    resolve: {
      budgetItem: BudgetItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default budgetItemRoute;
