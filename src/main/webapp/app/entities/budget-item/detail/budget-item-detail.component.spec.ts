import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { BudgetItemDetailComponent } from './budget-item-detail.component';

describe('BudgetItem Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BudgetItemDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: BudgetItemDetailComponent,
              resolve: { budgetItem: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(BudgetItemDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load budgetItem on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', BudgetItemDetailComponent);

      // THEN
      expect(instance.budgetItem).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
