import { IBudgetItem, NewBudgetItem } from './budget-item.model';

export const sampleWithRequiredData: IBudgetItem = {
  id: 22423,
  name: 'restituer tant ensemble',
  order: 13422,
};

export const sampleWithPartialData: IBudgetItem = {
  id: 8158,
  name: 'parlementaire reculer minuscule',
  order: 21953,
};

export const sampleWithFullData: IBudgetItem = {
  id: 189,
  name: 'alors que prestataire de services',
  order: 8534,
};

export const sampleWithNewData: NewBudgetItem = {
  name: 'partenaire via par suite de',
  order: 26459,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
