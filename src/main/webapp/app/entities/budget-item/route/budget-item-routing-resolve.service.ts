import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBudgetItem } from '../budget-item.model';
import { BudgetItemService } from '../service/budget-item.service';

export const budgetItemResolve = (route: ActivatedRouteSnapshot): Observable<null | IBudgetItem> => {
  const id = route.params['id'];
  if (id) {
    return inject(BudgetItemService)
      .find(id)
      .pipe(
        mergeMap((budgetItem: HttpResponse<IBudgetItem>) => {
          if (budgetItem.body) {
            return of(budgetItem.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default budgetItemResolve;
