import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IAccountGroup, NewAccountGroup } from '../account-group.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAccountGroup for edit and NewAccountGroupFormGroupInput for create.
 */
type AccountGroupFormGroupInput = IAccountGroup | PartialWithRequiredKeyOf<NewAccountGroup>;

type AccountGroupFormDefaults = Pick<NewAccountGroup, 'id' | 'defaultActive' | 'bankAccounts'>;

type AccountGroupFormGroupContent = {
  id: FormControl<IAccountGroup['id'] | NewAccountGroup['id']>;
  groupName: FormControl<IAccountGroup['groupName']>;
  defaultActive: FormControl<IAccountGroup['defaultActive']>;
  account: FormControl<IAccountGroup['account']>;
  bankAccounts: FormControl<IAccountGroup['bankAccounts']>;
};

export type AccountGroupFormGroup = FormGroup<AccountGroupFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AccountGroupFormService {
  createAccountGroupFormGroup(accountGroup: AccountGroupFormGroupInput = { id: null }): AccountGroupFormGroup {
    const accountGroupRawValue = {
      ...this.getFormDefaults(),
      ...accountGroup,
    };
    return new FormGroup<AccountGroupFormGroupContent>({
      id: new FormControl(
        { value: accountGroupRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      groupName: new FormControl(accountGroupRawValue.groupName, {
        validators: [Validators.required],
      }),
      defaultActive: new FormControl(accountGroupRawValue.defaultActive, {
        validators: [Validators.required],
      }),
      account: new FormControl(accountGroupRawValue.account, {
        validators: [Validators.required],
      }),
      bankAccounts: new FormControl(accountGroupRawValue.bankAccounts ?? []),
    });
  }

  getAccountGroup(form: AccountGroupFormGroup): IAccountGroup | NewAccountGroup {
    return form.getRawValue() as IAccountGroup | NewAccountGroup;
  }

  resetForm(form: AccountGroupFormGroup, accountGroup: AccountGroupFormGroupInput): void {
    const accountGroupRawValue = { ...this.getFormDefaults(), ...accountGroup };
    form.reset(
      {
        ...accountGroupRawValue,
        id: { value: accountGroupRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): AccountGroupFormDefaults {
    return {
      id: null,
      defaultActive: false,
      bankAccounts: [],
    };
  }
}
