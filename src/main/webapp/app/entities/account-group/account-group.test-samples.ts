import { IAccountGroup, NewAccountGroup } from './account-group.model';

export const sampleWithRequiredData: IAccountGroup = {
  id: 17278,
  groupName: 'traverser',
  defaultActive: false,
};

export const sampleWithPartialData: IAccountGroup = {
  id: 22863,
  groupName: 'vouloir jamais',
  defaultActive: true,
};

export const sampleWithFullData: IAccountGroup = {
  id: 29131,
  groupName: 'moderne échapper',
  defaultActive: false,
};

export const sampleWithNewData: NewAccountGroup = {
  groupName: 'atchoum',
  defaultActive: true,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
