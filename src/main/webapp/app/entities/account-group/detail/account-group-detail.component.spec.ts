import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { AccountGroupDetailComponent } from './account-group-detail.component';

describe('AccountGroup Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AccountGroupDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: AccountGroupDetailComponent,
              resolve: { accountGroup: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(AccountGroupDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load accountGroup on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', AccountGroupDetailComponent);

      // THEN
      expect(instance.accountGroup).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
