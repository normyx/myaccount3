import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAccountGroup } from '../account-group.model';
import { AccountGroupService } from '../service/account-group.service';

export const accountGroupResolve = (route: ActivatedRouteSnapshot): Observable<null | IAccountGroup> => {
  const id = route.params['id'];
  if (id) {
    return inject(AccountGroupService)
      .find(id)
      .pipe(
        mergeMap((accountGroup: HttpResponse<IAccountGroup>) => {
          if (accountGroup.body) {
            return of(accountGroup.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default accountGroupResolve;
