import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { AccountGroupComponent } from './list/account-group.component';
import { AccountGroupDetailComponent } from './detail/account-group-detail.component';
import { AccountGroupUpdateComponent } from './update/account-group-update.component';
import AccountGroupResolve from './route/account-group-routing-resolve.service';

const accountGroupRoute: Routes = [
  {
    path: '',
    component: AccountGroupComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AccountGroupDetailComponent,
    resolve: {
      accountGroup: AccountGroupResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AccountGroupUpdateComponent,
    resolve: {
      accountGroup: AccountGroupResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AccountGroupUpdateComponent,
    resolve: {
      accountGroup: AccountGroupResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default accountGroupRoute;
