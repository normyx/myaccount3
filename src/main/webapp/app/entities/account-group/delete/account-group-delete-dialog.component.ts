import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IAccountGroup } from '../account-group.model';
import { AccountGroupService } from '../service/account-group.service';

@Component({
  standalone: true,
  templateUrl: './account-group-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class AccountGroupDeleteDialogComponent {
  accountGroup?: IAccountGroup;

  constructor(
    protected accountGroupService: AccountGroupService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.accountGroupService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
