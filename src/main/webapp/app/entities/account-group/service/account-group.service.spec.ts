import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IAccountGroup } from '../account-group.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../account-group.test-samples';

import { AccountGroupService } from './account-group.service';

const requireRestSample: IAccountGroup = {
  ...sampleWithRequiredData,
};

describe('AccountGroup Service', () => {
  let service: AccountGroupService;
  let httpMock: HttpTestingController;
  let expectedResult: IAccountGroup | IAccountGroup[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AccountGroupService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a AccountGroup', () => {
      const accountGroup = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(accountGroup).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a AccountGroup', () => {
      const accountGroup = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(accountGroup).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a AccountGroup', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AccountGroup', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a AccountGroup', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    it('should handle exceptions for searching a AccountGroup', () => {
      const queryObject: any = {
        page: 0,
        size: 20,
        query: '',
        sort: [],
      };
      service.search(queryObject).subscribe(() => expectedResult);

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(null, { status: 500, statusText: 'Internal Server Error' });
      expect(expectedResult).toBe(null);
    });

    describe('addAccountGroupToCollectionIfMissing', () => {
      it('should add a AccountGroup to an empty array', () => {
        const accountGroup: IAccountGroup = sampleWithRequiredData;
        expectedResult = service.addAccountGroupToCollectionIfMissing([], accountGroup);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(accountGroup);
      });

      it('should not add a AccountGroup to an array that contains it', () => {
        const accountGroup: IAccountGroup = sampleWithRequiredData;
        const accountGroupCollection: IAccountGroup[] = [
          {
            ...accountGroup,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addAccountGroupToCollectionIfMissing(accountGroupCollection, accountGroup);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AccountGroup to an array that doesn't contain it", () => {
        const accountGroup: IAccountGroup = sampleWithRequiredData;
        const accountGroupCollection: IAccountGroup[] = [sampleWithPartialData];
        expectedResult = service.addAccountGroupToCollectionIfMissing(accountGroupCollection, accountGroup);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(accountGroup);
      });

      it('should add only unique AccountGroup to an array', () => {
        const accountGroupArray: IAccountGroup[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const accountGroupCollection: IAccountGroup[] = [sampleWithRequiredData];
        expectedResult = service.addAccountGroupToCollectionIfMissing(accountGroupCollection, ...accountGroupArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const accountGroup: IAccountGroup = sampleWithRequiredData;
        const accountGroup2: IAccountGroup = sampleWithPartialData;
        expectedResult = service.addAccountGroupToCollectionIfMissing([], accountGroup, accountGroup2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(accountGroup);
        expect(expectedResult).toContain(accountGroup2);
      });

      it('should accept null and undefined values', () => {
        const accountGroup: IAccountGroup = sampleWithRequiredData;
        expectedResult = service.addAccountGroupToCollectionIfMissing([], null, accountGroup, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(accountGroup);
      });

      it('should return initial array if no AccountGroup is added', () => {
        const accountGroupCollection: IAccountGroup[] = [sampleWithRequiredData];
        expectedResult = service.addAccountGroupToCollectionIfMissing(accountGroupCollection, undefined, null);
        expect(expectedResult).toEqual(accountGroupCollection);
      });
    });

    describe('compareAccountGroup', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareAccountGroup(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareAccountGroup(entity1, entity2);
        const compareResult2 = service.compareAccountGroup(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareAccountGroup(entity1, entity2);
        const compareResult2 = service.compareAccountGroup(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareAccountGroup(entity1, entity2);
        const compareResult2 = service.compareAccountGroup(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
