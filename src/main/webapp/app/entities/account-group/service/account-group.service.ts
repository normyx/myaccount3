import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, asapScheduler, scheduled } from 'rxjs';

import { catchError } from 'rxjs/operators';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { SearchWithPagination } from 'app/core/request/request.model';
import { IAccountGroup, NewAccountGroup } from '../account-group.model';

export type PartialUpdateAccountGroup = Partial<IAccountGroup> & Pick<IAccountGroup, 'id'>;

export type EntityResponseType = HttpResponse<IAccountGroup>;
export type EntityArrayResponseType = HttpResponse<IAccountGroup[]>;

@Injectable({ providedIn: 'root' })
export class AccountGroupService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/account-groups');
  protected resourceSearchUrl = this.applicationConfigService.getEndpointFor('api/account-groups/_search');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(accountGroup: NewAccountGroup): Observable<EntityResponseType> {
    return this.http.post<IAccountGroup>(this.resourceUrl, accountGroup, { observe: 'response' });
  }

  update(accountGroup: IAccountGroup): Observable<EntityResponseType> {
    return this.http.put<IAccountGroup>(`${this.resourceUrl}/${this.getAccountGroupIdentifier(accountGroup)}`, accountGroup, {
      observe: 'response',
    });
  }

  partialUpdate(accountGroup: PartialUpdateAccountGroup): Observable<EntityResponseType> {
    return this.http.patch<IAccountGroup>(`${this.resourceUrl}/${this.getAccountGroupIdentifier(accountGroup)}`, accountGroup, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAccountGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAccountGroup[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAccountGroup[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(catchError(() => scheduled([new HttpResponse<IAccountGroup[]>()], asapScheduler)));
  }

  getAccountGroupIdentifier(accountGroup: Pick<IAccountGroup, 'id'>): number {
    return accountGroup.id;
  }

  compareAccountGroup(o1: Pick<IAccountGroup, 'id'> | null, o2: Pick<IAccountGroup, 'id'> | null): boolean {
    return o1 && o2 ? this.getAccountGroupIdentifier(o1) === this.getAccountGroupIdentifier(o2) : o1 === o2;
  }

  addAccountGroupToCollectionIfMissing<Type extends Pick<IAccountGroup, 'id'>>(
    accountGroupCollection: Type[],
    ...accountGroupsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const accountGroups: Type[] = accountGroupsToCheck.filter(isPresent);
    if (accountGroups.length > 0) {
      const accountGroupCollectionIdentifiers = accountGroupCollection.map(
        accountGroupItem => this.getAccountGroupIdentifier(accountGroupItem)!,
      );
      const accountGroupsToAdd = accountGroups.filter(accountGroupItem => {
        const accountGroupIdentifier = this.getAccountGroupIdentifier(accountGroupItem);
        if (accountGroupCollectionIdentifiers.includes(accountGroupIdentifier)) {
          return false;
        }
        accountGroupCollectionIdentifiers.push(accountGroupIdentifier);
        return true;
      });
      return [...accountGroupsToAdd, ...accountGroupCollection];
    }
    return accountGroupCollection;
  }
}
