import { Routes } from '@angular/router';
import { MyaDashboardAccountComponent } from './account/mya-dashboard-account.component';
import { MyaDashboardCategoryComponent } from './category/category/mya-dashboard-category.component';
import myaCategoryResolve from './category/route/mya-dashboard-category-routing-resolve.service';

const myaDashBoardRoute: Routes = [
  {
    path: 'account',
    component: MyaDashboardAccountComponent,
  },
  {
    path: 'category/:id/:month',
    component: MyaDashboardCategoryComponent,
    resolve: {
      category: myaCategoryResolve,
    },
  },
  /*
  {
    path: 'category/details/:id/:month',
    component: MyaDashboardCategoryDetailsComponent,
    resolve: {
      category: MyaDashboardCategoryRoutingResolveService,
    },
  },*/
];

export default myaDashBoardRoute;
