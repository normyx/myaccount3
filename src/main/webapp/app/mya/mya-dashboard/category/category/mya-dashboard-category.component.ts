import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Component, OnInit } from '@angular/core';

import { ActivatedRoute, Params, RouterModule } from '@angular/router';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ICategory } from 'app/entities/category/category.model';
import { CategoryService } from 'app/entities/category/service/category.service';
import { MYA_REPORT_NUMBER_OF_MONTHS_FOR_RANGES } from 'app/mya/config/mya.constants';
import SharedModule from 'app/shared/shared.module';
import dayjs, { Dayjs } from 'dayjs';
import { BaseChartDirective } from 'ng2-charts';
import { BsDatepickerConfig, BsDatepickerModule, BsDatepickerViewMode } from 'ngx-bootstrap/datepicker';
import { MyaCategorySplitComponent } from '../../graphic-components/mya-category-split/mya-category-split.component';
import { MyaCategoryWeatherComponent } from '../../graphic-components/mya-category-weather/mya-category-weather.component';
import { MyaEvolutionByMonthsWithSmoothedAndMarkedComponent } from '../../graphic-components/mya-evolution-by-months-with-smoothed-and-marked/mya-evolution-by-months-with-smoothed-and-marked.component';
import { MyaEvolutionByMonthsComponent } from '../../graphic-components/mya-evolution-by-months/mya-evolution-by-months.component';
import { MyaEvolutionInMonthReportComponent } from '../../graphic-components/mya-evolution-in-month/mya-evolution-in-month.component';
import { FormsModule } from '@angular/forms';
import { MyaCategorySummaryComponent } from '../mya-category-summary/mya-caterory-summary.component';
import { MyaBudgetItemListComponent } from 'app/mya/mya-budget-item/list/mya-budget-item-list.component';
import { MyaOperationListComponent } from 'app/mya/mya-operation/list/mya-operation-list.component';

@Component({
  selector: 'jhi-mya-dashboard-category',
  templateUrl: './mya-dashboard-category.component.html',
  standalone: true,
  imports: [
    SharedModule,
    BaseChartDirective,
    RouterModule,
    FormsModule,
    BsDatepickerModule,
    MyaCategorySummaryComponent,
    MyaBudgetItemListComponent,
    MyaOperationListComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MyaDashboardCategoryComponent implements OnInit {
  currentMonth: Date;

  selectedMonthMinusNumberOfMonth: Date = new Date();
  category: ICategory | null = null;
  selectedCategoryId: number | null = null;
  categories: ICategory[] | null = null;
  selectedMonth: Date = new Date();
  bsConfig?: Partial<BsDatepickerConfig>;
  minMode: BsDatepickerViewMode = 'month';
  selectedMonthLastDay: Date = new Date();

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected categoryService: CategoryService,
  ) {
    const currentDate: Dayjs = dayjs(Date.now());
    this.currentMonth = new Date(currentDate.year(), currentDate.month(), 1);
    this.selectedMonth = this.currentMonth;
    this.setSelectedMonth();
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['month']) {
        this.selectedMonth = dayjs(params['month']).toDate();
        this.setSelectedMonth();
      }
    });
    this.activatedRoute.data.subscribe(({ category }) => {
      if (category) {
        this.category = category;
        this.selectedCategoryId = category.id;
      }
    });
    this.categoryService.query().subscribe((res: HttpResponse<ICategory[]>) => (this.categories = res.body!));
    this.bsConfig = Object.assign(
      {},
      {
        minMode: this.minMode,
        containerClass: 'theme-default',
        isAnimated: true,
        dateInputFormat: 'MMM-YY',
      },
    );
  }

  setSelectedMonth(): void {
    this.selectedMonthMinusNumberOfMonth = new Date(
      this.selectedMonth.getFullYear(),
      this.selectedMonth.getMonth() - MYA_REPORT_NUMBER_OF_MONTHS_FOR_RANGES,
      1,
    );
    this.selectedMonthLastDay = dayjs(this.selectedMonth).clone().add(1, 'month').add(-1, 'day').toDate();
  }

  getMonthStr(): string | null {
    return dayjs(this.selectedMonth).format(DATE_FORMAT);
  }
}
