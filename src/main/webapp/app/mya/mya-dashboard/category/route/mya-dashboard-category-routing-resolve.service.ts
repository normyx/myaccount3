import { HttpResponse } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { ICategory } from 'app/entities/category/category.model';
import { CategoryService } from 'app/entities/category/service/category.service';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

export const myaCategoryResolve = (route: ActivatedRouteSnapshot): Observable<null | ICategory> => {
  const id = route.params['id'];
  if (id) {
    return inject(CategoryService)
      .find(id)
      .pipe(
        mergeMap((category: HttpResponse<ICategory>) => {
          if (category.body) {
            return of(category.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default myaCategoryResolve;
