import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Component, Input, OnChanges } from '@angular/core';
import dayjs from 'dayjs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DATE_FORMAT } from '../../../../config/input.constants';
import { ICategory } from '../../../../entities/category/category.model';
import { CategoryService } from '../../../../entities/category/service/category.service';
import SharedModule from 'app/shared/shared.module';
import { BaseChartDirective } from 'ng2-charts';
import { RouterModule } from '@angular/router';
import { MyaCategoryWeatherComponent } from '../../graphic-components/mya-category-weather/mya-category-weather.component';
import { MyaCategorySplitComponent } from '../../graphic-components/mya-category-split/mya-category-split.component';
import { MyaEvolutionByMonthsComponent } from '../../graphic-components/mya-evolution-by-months/mya-evolution-by-months.component';
import { MyaEvolutionByMonthsWithSmoothedAndMarkedComponent } from '../../graphic-components/mya-evolution-by-months-with-smoothed-and-marked/mya-evolution-by-months-with-smoothed-and-marked.component';
import { MyaEvolutionInMonthReportComponent } from '../../graphic-components/mya-evolution-in-month/mya-evolution-in-month.component';

@Component({
  selector: 'jhi-mya-category-summary',
  templateUrl: './mya-caterory-summary.component.html',
  standalone: true,
  imports: [
    SharedModule,
    BaseChartDirective,
    RouterModule,
    MyaCategoryWeatherComponent,
    MyaCategorySplitComponent,
    MyaEvolutionByMonthsComponent,
    MyaEvolutionByMonthsWithSmoothedAndMarkedComponent,
    MyaEvolutionInMonthReportComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MyaCategorySummaryComponent implements OnChanges {
  @Input() categoryId: number | null = null;
  @Input() monthTo: Date | null = null;
  @Input() monthFrom: Date | null = null;
  @Input() isSummary = true;

  category: Observable<ICategory | null> | null = null;

  constructor(private categoryService: CategoryService) {}

  loadAll(): void {
    if (this.categoryId) {
      this.category = this.categoryService.find(this.categoryId).pipe(map((res: HttpResponse<ICategory>) => res.body));
    }
  }

  ngOnChanges(): void {
    this.loadAll();
  }

  getMonthStr(): string | null {
    if (this.monthTo) {
      return dayjs(this.monthTo).format(DATE_FORMAT);
    } else {
      return null;
    }
  }
}
