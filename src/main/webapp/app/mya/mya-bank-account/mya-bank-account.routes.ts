import { Routes } from '@angular/router';
import { MyaBankAccountListComponent } from './list/mya-bank-account-list.component';
import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MyaCurrentBankAccountSummaryComponent } from './summary/mya-current-bank-account-summary.component';
import { DESC } from 'app/config/navigation.constants';
import bankAccountResolve from 'app/entities/bank-account/route/bank-account-routing-resolve.service';
import { MyaSavingsBankAccountSummaryComponent } from './summary/mya-savings-bank-account-summary.component';
import { MyaRealEstateBankAccountSummaryComponent } from './summary/mya-real-estate-bank-account-summary.component';
import { MyaPortfolioBankAccountSummaryComponent } from './summary/mya-portfolio-bank-account-summary.component';

const myaBankAccountRoute: Routes = [
  {
    path: '',
    component: MyaBankAccountListComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/current-detail',
    component: MyaCurrentBankAccountSummaryComponent,
    resolve: {
      bankAccount: bankAccountResolve,
    },
    data: {
      defaultSort: 'date,' + DESC,
    },
    canActivate: [UserRouteAccessService],
  },

  {
    path: ':id/savings-detail',
    component: MyaSavingsBankAccountSummaryComponent,
    resolve: {
      bankAccount: bankAccountResolve,
    },
    data: {
      defaultSort: 'date,' + DESC,
    },
    canActivate: [UserRouteAccessService],
  },

  {
    path: ':id/real-estate-detail',
    component: MyaRealEstateBankAccountSummaryComponent,
    resolve: {
      bankAccount: bankAccountResolve,
    },
    data: {
      defaultSort: 'date,' + DESC,
    },
    canActivate: [UserRouteAccessService],
  },

  {
    path: ':id/portfolio-detail',
    component: MyaPortfolioBankAccountSummaryComponent,
    resolve: {
      bankAccount: bankAccountResolve,
    },
    data: {
      defaultSort: 'date,' + DESC,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default myaBankAccountRoute;
