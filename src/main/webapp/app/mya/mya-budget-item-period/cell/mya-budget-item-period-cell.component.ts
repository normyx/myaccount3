import { CUSTOM_ELEMENTS_SCHEMA, Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import SharedModule from 'app/shared/shared.module';
import { IBudgetItemPeriod } from '../../../entities/budget-item-period/budget-item-period.model';
import { IBudgetItem } from '../../../entities/budget-item/budget-item.model';
import { MyaBudgetItemPeriodCellElementComponent } from './mya-budget-item-period-cell-element.component';

@Component({
  standalone: true,
  /* eslint-disable @angular-eslint/component-selector */
  selector: '[jhi-mya-budget-item-period-cell]',
  templateUrl: './mya-budget-item-period-cell.component.html',
  styleUrls: ['./mya-budget-item-period-cell.component.scss'],
  imports: [SharedModule, MyaBudgetItemPeriodCellElementComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MyaBudgetItemPeriodCellComponent {
  @Input() budgetItemPeriods: IBudgetItemPeriod[] | null = null;
  @Input() budgetItem: IBudgetItem | null = null;
  @Input() editable: boolean | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}
}
