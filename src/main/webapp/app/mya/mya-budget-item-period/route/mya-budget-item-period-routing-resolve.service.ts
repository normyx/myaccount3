import { HttpResponse } from '@angular/common/http';
import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { IBudgetItemPeriod } from '../../../entities/budget-item-period/budget-item-period.model';
import { BudgetItemPeriodService } from '../../../entities/budget-item-period/service/budget-item-period.service';

export const myaBudgetItemPeriodResolve = (route: ActivatedRouteSnapshot): Observable<null | IBudgetItemPeriod> => {
  const id = route.params['id'];
  if (id) {
    return inject(BudgetItemPeriodService)
      .find(id)
      .pipe(
        mergeMap((budgetItemPeriod: HttpResponse<IBudgetItemPeriod>) => {
          if (budgetItemPeriod.body) {
            return of(budgetItemPeriod.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default myaBudgetItemPeriodResolve;
