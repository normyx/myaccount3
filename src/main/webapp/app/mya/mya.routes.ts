import { Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'mya-operation',
    data: { pageTitle: 'mya.entity.operation.title' },
    loadChildren: () => import('./mya-operation/mya-operation.routes'),
  },
  {
    path: 'mya-budget-item',
    data: { pageTitle: 'mya.entity.budgetItem.title' },
    loadChildren: () => import('./mya-budget-item/mya-budget-item.routes'),
  },
  {
    path: 'mya-dashboard',
    data: { pageTitle: 'mya.dashboard.title' },
    loadChildren: () => import('./mya-dashboard/mya-dashboard.routes'),
  },
  {
    path: 'mya-bank-account',
    data: { pageTitle: 'mya.entity.bankAccount.title' },
    loadChildren: () => import('./mya-bank-account/mya-bank-account.routes'),
  },
];
export default routes;
