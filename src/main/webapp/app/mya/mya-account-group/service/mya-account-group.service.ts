import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IAccountGroup } from 'app/entities/account-group/account-group.model';
import { AccountGroupService } from 'app/entities/account-group/service/account-group.service';
import { Observable } from 'rxjs';
import { ApplicationConfigService } from '../../../core/config/application-config.service';
import { createRequestOption } from '../../../core/request/request-util';
import { EntityArrayResponseType } from '../../../entities/bank-account/service/bank-account.service';

@Injectable({ providedIn: 'root' })
export class MyaAccountGroupService extends AccountGroupService {
  static readonly MYA_ACCOUNT_GROUP_API = 'api/mya/account-groups';
  static readonly MYA_WITH_SIGNEDIN_USER_SUFFIX = '/with-signedin-user';
  protected myaResourceUrl = this.applicationConfigService.getEndpointFor(MyaAccountGroupService.MYA_ACCOUNT_GROUP_API);
  protected myaResourceFromSignedInUserUrl = this.applicationConfigService.getEndpointFor(
    MyaAccountGroupService.MYA_ACCOUNT_GROUP_API + MyaAccountGroupService.MYA_WITH_SIGNEDIN_USER_SUFFIX,
  );

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {
    super(http, applicationConfigService);
  }

  queryWithSignedInUser(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAccountGroup[]>(this.myaResourceFromSignedInUserUrl, { params: options, observe: 'response' });
  }
}
