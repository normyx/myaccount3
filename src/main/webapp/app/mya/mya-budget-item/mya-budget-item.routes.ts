import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../core/auth/user-route-access.service';

import { ASC } from '../../config/navigation.constants';
import { MyaBudgetItemListPageComponent } from './list/mya-budget-item-list-page.component';

const myaBudgetItemRoute: Routes = [
  {
    path: '',
    component: MyaBudgetItemListPageComponent,
    data: {
      defaultSort: 'order,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default myaBudgetItemRoute;
