import { HttpResponse } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { IBudgetItem } from '../../../entities/budget-item/budget-item.model';
import { BudgetItemService } from '../../../entities/budget-item/service/budget-item.service';

export const myaBudgetItemResolve = (route: ActivatedRouteSnapshot): Observable<null | IBudgetItem> => {
  const id = route.params['id'];
  if (id) {
    return inject(BudgetItemService)
      .find(id)
      .pipe(
        mergeMap((category: HttpResponse<IBudgetItem>) => {
          if (category.body) {
            return of(category.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default myaBudgetItemResolve;
