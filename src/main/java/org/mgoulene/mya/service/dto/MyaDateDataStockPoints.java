package org.mgoulene.mya.service.dto;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.mgoulene.domain.StockMarketData;
import org.mgoulene.domain.enumeration.Currency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyaDateDataStockPoints extends MyaDateDataPoints<MyaDateDataPointsStockProperties, MyaDateDataStockPointData> {

    private final Logger log = LoggerFactory.getLogger(MyaDateDataStockPoints.class);

    public MyaDateDataStockPoints(
        List<StockMarketData> stockDatas,
        String symbol,
        Currency currency,
        float numberOfShare,
        LocalDate acquisitionDate,
        float acquisitionPrice,
        LocalDate sellDate,
        MyaStockDataList currencyDatas
    ) {
        super(new MyaDateDataPointsStockProperties(symbol, currency));
        // First Filter stock data from acquisitionDate
        List<StockMarketData> filteredStockMarketData;
        if (sellDate != null) {
            filteredStockMarketData =
                stockDatas
                    .stream()
                    .filter(stockMarketData ->
                        (stockMarketData.getDataDate().isAfter(acquisitionDate) ||
                            stockMarketData.getDataDate().isEqual(acquisitionDate)) &&
                        (stockMarketData.getDataDate().isBefore(sellDate) || stockMarketData.getDataDate().isEqual(sellDate))
                    )
                    .collect(Collectors.toList());
        } else {
            filteredStockMarketData =
                stockDatas
                    .stream()
                    .filter(stockMarketData ->
                        stockMarketData.getDataDate().isAfter(acquisitionDate) || stockMarketData.getDataDate().isEqual(acquisitionDate)
                    )
                    .collect(Collectors.toList());
        }
        StockMarketData initialCurrencyData = null;
        if (currency != null && currency != Currency.EUR) {
            Optional<StockMarketData> initialCurrencyDataOpt = currencyDatas.getFromDate(acquisitionDate);
            if (initialCurrencyDataOpt.isPresent()) {
                initialCurrencyData = initialCurrencyDataOpt.orElseThrow();
            }
        }
        for (StockMarketData stockData : filteredStockMarketData) {
            StockMarketData currentCurrencyData = null;
            if (currency != null && currency != Currency.EUR) {
                Optional<StockMarketData> currentCurrencyDataOpt = currencyDatas.getFromDate(stockData.getDataDate());
                if (currentCurrencyDataOpt.isPresent()) {
                    currentCurrencyData = currentCurrencyDataOpt.orElseThrow();
                }
            }
            addPoint(
                sellDate != null && sellDate.isEqual(stockData.getDataDate())
                    ? new MyaDateDataPoint<>(
                        stockData.getDataDate(),
                        new MyaDateDataStockPointData(
                            this.getProperties(),
                            null,
                            0,
                            acquisitionPrice,
                            currentCurrencyData,
                            initialCurrencyData
                        )
                    )
                    : new MyaDateDataPoint<>(
                        stockData.getDataDate(),
                        new MyaDateDataStockPointData(
                            this.getProperties(),
                            stockData,
                            numberOfShare,
                            acquisitionPrice,
                            currentCurrencyData,
                            initialCurrencyData
                        )
                    )
            );
        }
    }

    public MyaDateDataSinglePoints toSimplePoints() {
        MyaDateDataSinglePoints singlePoints = new MyaDateDataSinglePoints(null);
        for (MyaDateDataPoint<MyaDateDataStockPointData> point : this.getPoints()) {
            singlePoints.addPoint(
                new MyaDateDataPoint<MyaDateDataSinglePointData>(
                    point.getDate(),
                    new MyaDateDataSinglePointData(point.getData().getValueInEuro())
                )
            );
        }
        return singlePoints;
    }
}
