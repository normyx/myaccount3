package org.mgoulene.mya.service.mapper;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import org.mgoulene.mya.service.dto.OperationCSVDTO;
import org.mgoulene.service.dto.OperationDTO;
import org.springframework.stereotype.Component;

@Component
public class OperationCSVToDTO {

    public List<OperationDTO> toDto(List<OperationCSVDTO> entityList) {
        if (entityList == null) {
            return null;
        }

        List<OperationDTO> list = new ArrayList<OperationDTO>(entityList.size());
        for (OperationCSVDTO operationCSVDTO : entityList) {
            list.add(toDto(operationCSVDTO));
        }

        return list;
    }

    public OperationDTO toDto(OperationCSVDTO operation) {
        if (operation == null) {
            return null;
        }

        OperationDTO operationDTO = new OperationDTO();

        if (operation.getDate() != null) {
            // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            // LocalDate date = java.time.LocalDate.parse(operation.getDate(), formatter);
            // operationDTO.setDate(date);
        }
        if (operation.getAmount() != null) {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            DecimalFormat format = new DecimalFormat("0.#");
            format.setDecimalFormatSymbols(symbols);
            /*
             * try {
             * operationDTO.setAmount(format.parse(operation.getAmount()).floatValue());
             * } catch (ParseException e) {
             * // TODO Auto-generated catch block
             * e.printStackTrace();
             * }
             */
        }
        operationDTO.setLabel(operation.getLabel());
        operationDTO.setNote(operation.getNote());
        operationDTO.setCheckNumber(operation.getCheckNumber());

        return operationDTO;
    }
}
