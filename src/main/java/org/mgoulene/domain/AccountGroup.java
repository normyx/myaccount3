package org.mgoulene.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A AccountGroup.
 */
@Entity
@Table(name = "account_group")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "accountgroup")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AccountGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "group_name", nullable = false)
    @org.springframework.data.elasticsearch.annotations.Field(type = org.springframework.data.elasticsearch.annotations.FieldType.Text)
    private String groupName;

    @NotNull
    @Column(name = "default_active", nullable = false)
    @org.springframework.data.elasticsearch.annotations.Field(type = org.springframework.data.elasticsearch.annotations.FieldType.Boolean)
    private Boolean defaultActive;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "user" }, allowSetters = true)
    private ApplicationUser account;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "rel_account_group__bank_account",
        joinColumns = @JoinColumn(name = "account_group_id"),
        inverseJoinColumns = @JoinColumn(name = "bank_account_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "realEstateItems", "stockPortfolioItems", "account", "accountGroups" }, allowSetters = true)
    private Set<BankAccount> bankAccounts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public AccountGroup id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public AccountGroup groupName(String groupName) {
        this.setGroupName(groupName);
        return this;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Boolean getDefaultActive() {
        return this.defaultActive;
    }

    public AccountGroup defaultActive(Boolean defaultActive) {
        this.setDefaultActive(defaultActive);
        return this;
    }

    public void setDefaultActive(Boolean defaultActive) {
        this.defaultActive = defaultActive;
    }

    public ApplicationUser getAccount() {
        return this.account;
    }

    public void setAccount(ApplicationUser applicationUser) {
        this.account = applicationUser;
    }

    public AccountGroup account(ApplicationUser applicationUser) {
        this.setAccount(applicationUser);
        return this;
    }

    public Set<BankAccount> getBankAccounts() {
        return this.bankAccounts;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public AccountGroup bankAccounts(Set<BankAccount> bankAccounts) {
        this.setBankAccounts(bankAccounts);
        return this;
    }

    public AccountGroup addBankAccount(BankAccount bankAccount) {
        this.bankAccounts.add(bankAccount);
        return this;
    }

    public AccountGroup removeBankAccount(BankAccount bankAccount) {
        this.bankAccounts.remove(bankAccount);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccountGroup)) {
            return false;
        }
        return getId() != null && getId().equals(((AccountGroup) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AccountGroup{" +
            "id=" + getId() +
            ", groupName='" + getGroupName() + "'" +
            ", defaultActive='" + getDefaultActive() + "'" +
            "}";
    }
}
