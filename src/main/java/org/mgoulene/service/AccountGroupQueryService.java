package org.mgoulene.service;

import jakarta.persistence.criteria.JoinType;
import java.util.List;
import org.mgoulene.domain.*; // for static metamodels
import org.mgoulene.domain.AccountGroup;
import org.mgoulene.repository.AccountGroupRepository;
import org.mgoulene.repository.search.AccountGroupSearchRepository;
import org.mgoulene.service.criteria.AccountGroupCriteria;
import org.mgoulene.service.dto.AccountGroupDTO;
import org.mgoulene.service.mapper.AccountGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link AccountGroup} entities in the database.
 * The main input is a {@link AccountGroupCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AccountGroupDTO} or a {@link Page} of {@link AccountGroupDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AccountGroupQueryService extends QueryService<AccountGroup> {

    private final Logger log = LoggerFactory.getLogger(AccountGroupQueryService.class);

    private final AccountGroupRepository accountGroupRepository;

    private final AccountGroupMapper accountGroupMapper;

    private final AccountGroupSearchRepository accountGroupSearchRepository;

    public AccountGroupQueryService(
        AccountGroupRepository accountGroupRepository,
        AccountGroupMapper accountGroupMapper,
        AccountGroupSearchRepository accountGroupSearchRepository
    ) {
        this.accountGroupRepository = accountGroupRepository;
        this.accountGroupMapper = accountGroupMapper;
        this.accountGroupSearchRepository = accountGroupSearchRepository;
    }

    /**
     * Return a {@link List} of {@link AccountGroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AccountGroupDTO> findByCriteria(AccountGroupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AccountGroup> specification = createSpecification(criteria);
        return accountGroupMapper.toDto(accountGroupRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AccountGroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AccountGroupDTO> findByCriteria(AccountGroupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AccountGroup> specification = createSpecification(criteria);
        return accountGroupRepository.findAll(specification, page).map(accountGroupMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AccountGroupCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AccountGroup> specification = createSpecification(criteria);
        return accountGroupRepository.count(specification);
    }

    /**
     * Function to convert {@link AccountGroupCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AccountGroup> createSpecification(AccountGroupCriteria criteria) {
        Specification<AccountGroup> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), AccountGroup_.id));
            }
            if (criteria.getGroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGroupName(), AccountGroup_.groupName));
            }
            if (criteria.getDefaultActive() != null) {
                specification = specification.and(buildSpecification(criteria.getDefaultActive(), AccountGroup_.defaultActive));
            }
            if (criteria.getAccountId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAccountId(),
                            root -> root.join(AccountGroup_.account, JoinType.LEFT).get(ApplicationUser_.id)
                        )
                    );
            }
            if (criteria.getBankAccountId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getBankAccountId(),
                            root -> root.join(AccountGroup_.bankAccounts, JoinType.LEFT).get(BankAccount_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
