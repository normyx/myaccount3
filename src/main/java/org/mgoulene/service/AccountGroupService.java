package org.mgoulene.service;

import java.util.Optional;
import org.mgoulene.domain.AccountGroup;
import org.mgoulene.repository.AccountGroupRepository;
import org.mgoulene.repository.search.AccountGroupSearchRepository;
import org.mgoulene.service.dto.AccountGroupDTO;
import org.mgoulene.service.mapper.AccountGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link org.mgoulene.domain.AccountGroup}.
 */
@Service
@Transactional
public class AccountGroupService {

    private final Logger log = LoggerFactory.getLogger(AccountGroupService.class);

    private final AccountGroupRepository accountGroupRepository;

    private final AccountGroupMapper accountGroupMapper;

    private final AccountGroupSearchRepository accountGroupSearchRepository;

    public AccountGroupService(
        AccountGroupRepository accountGroupRepository,
        AccountGroupMapper accountGroupMapper,
        AccountGroupSearchRepository accountGroupSearchRepository
    ) {
        this.accountGroupRepository = accountGroupRepository;
        this.accountGroupMapper = accountGroupMapper;
        this.accountGroupSearchRepository = accountGroupSearchRepository;
    }

    /**
     * Save a accountGroup.
     *
     * @param accountGroupDTO the entity to save.
     * @return the persisted entity.
     */
    public AccountGroupDTO save(AccountGroupDTO accountGroupDTO) {
        log.debug("Request to save AccountGroup : {}", accountGroupDTO);
        AccountGroup accountGroup = accountGroupMapper.toEntity(accountGroupDTO);
        accountGroup = accountGroupRepository.save(accountGroup);
        AccountGroupDTO result = accountGroupMapper.toDto(accountGroup);
        accountGroupSearchRepository.index(accountGroup);
        return result;
    }

    /**
     * Update a accountGroup.
     *
     * @param accountGroupDTO the entity to save.
     * @return the persisted entity.
     */
    public AccountGroupDTO update(AccountGroupDTO accountGroupDTO) {
        log.debug("Request to update AccountGroup : {}", accountGroupDTO);
        AccountGroup accountGroup = accountGroupMapper.toEntity(accountGroupDTO);
        accountGroup = accountGroupRepository.save(accountGroup);
        AccountGroupDTO result = accountGroupMapper.toDto(accountGroup);
        accountGroupSearchRepository.index(accountGroup);
        return result;
    }

    /**
     * Partially update a accountGroup.
     *
     * @param accountGroupDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<AccountGroupDTO> partialUpdate(AccountGroupDTO accountGroupDTO) {
        log.debug("Request to partially update AccountGroup : {}", accountGroupDTO);

        return accountGroupRepository
            .findById(accountGroupDTO.getId())
            .map(existingAccountGroup -> {
                accountGroupMapper.partialUpdate(existingAccountGroup, accountGroupDTO);

                return existingAccountGroup;
            })
            .map(accountGroupRepository::save)
            .map(savedAccountGroup -> {
                accountGroupSearchRepository.index(savedAccountGroup);
                return savedAccountGroup;
            })
            .map(accountGroupMapper::toDto);
    }

    /**
     * Get all the accountGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AccountGroupDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AccountGroups");
        return accountGroupRepository.findAll(pageable).map(accountGroupMapper::toDto);
    }

    /**
     * Get all the accountGroups with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<AccountGroupDTO> findAllWithEagerRelationships(Pageable pageable) {
        return accountGroupRepository.findAllWithEagerRelationships(pageable).map(accountGroupMapper::toDto);
    }

    /**
     * Get one accountGroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AccountGroupDTO> findOne(Long id) {
        log.debug("Request to get AccountGroup : {}", id);
        return accountGroupRepository.findOneWithEagerRelationships(id).map(accountGroupMapper::toDto);
    }

    /**
     * Delete the accountGroup by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AccountGroup : {}", id);
        accountGroupRepository.deleteById(id);
        accountGroupSearchRepository.deleteFromIndexById(id);
    }

    /**
     * Search for the accountGroup corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AccountGroupDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AccountGroups for query {}", query);
        return accountGroupSearchRepository.search(query, pageable).map(accountGroupMapper::toDto);
    }
}
