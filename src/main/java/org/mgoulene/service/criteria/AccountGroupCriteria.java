package org.mgoulene.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.core.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link org.mgoulene.domain.AccountGroup} entity. This class is used
 * in {@link org.mgoulene.web.rest.AccountGroupResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /account-groups?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AccountGroupCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter groupName;

    private BooleanFilter defaultActive;

    private LongFilter accountId;

    private LongFilter bankAccountId;

    private Boolean distinct;

    public AccountGroupCriteria() {}

    public AccountGroupCriteria(AccountGroupCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.groupName = other.groupName == null ? null : other.groupName.copy();
        this.defaultActive = other.defaultActive == null ? null : other.defaultActive.copy();
        this.accountId = other.accountId == null ? null : other.accountId.copy();
        this.bankAccountId = other.bankAccountId == null ? null : other.bankAccountId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public AccountGroupCriteria copy() {
        return new AccountGroupCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getGroupName() {
        return groupName;
    }

    public StringFilter groupName() {
        if (groupName == null) {
            groupName = new StringFilter();
        }
        return groupName;
    }

    public void setGroupName(StringFilter groupName) {
        this.groupName = groupName;
    }

    public BooleanFilter getDefaultActive() {
        return defaultActive;
    }

    public BooleanFilter defaultActive() {
        if (defaultActive == null) {
            defaultActive = new BooleanFilter();
        }
        return defaultActive;
    }

    public void setDefaultActive(BooleanFilter defaultActive) {
        this.defaultActive = defaultActive;
    }

    public LongFilter getAccountId() {
        return accountId;
    }

    public LongFilter accountId() {
        if (accountId == null) {
            accountId = new LongFilter();
        }
        return accountId;
    }

    public void setAccountId(LongFilter accountId) {
        this.accountId = accountId;
    }

    public LongFilter getBankAccountId() {
        return bankAccountId;
    }

    public LongFilter bankAccountId() {
        if (bankAccountId == null) {
            bankAccountId = new LongFilter();
        }
        return bankAccountId;
    }

    public void setBankAccountId(LongFilter bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AccountGroupCriteria that = (AccountGroupCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(groupName, that.groupName) &&
            Objects.equals(defaultActive, that.defaultActive) &&
            Objects.equals(accountId, that.accountId) &&
            Objects.equals(bankAccountId, that.bankAccountId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, groupName, defaultActive, accountId, bankAccountId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AccountGroupCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (groupName != null ? "groupName=" + groupName + ", " : "") +
            (defaultActive != null ? "defaultActive=" + defaultActive + ", " : "") +
            (accountId != null ? "accountId=" + accountId + ", " : "") +
            (bankAccountId != null ? "bankAccountId=" + bankAccountId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
