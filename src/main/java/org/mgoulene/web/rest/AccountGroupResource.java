package org.mgoulene.web.rest;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.mgoulene.repository.AccountGroupRepository;
import org.mgoulene.service.AccountGroupQueryService;
import org.mgoulene.service.AccountGroupService;
import org.mgoulene.service.criteria.AccountGroupCriteria;
import org.mgoulene.service.dto.AccountGroupDTO;
import org.mgoulene.web.rest.errors.BadRequestAlertException;
import org.mgoulene.web.rest.errors.ElasticsearchExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link org.mgoulene.domain.AccountGroup}.
 */
@RestController
@RequestMapping("/api/account-groups")
public class AccountGroupResource {

    private final Logger log = LoggerFactory.getLogger(AccountGroupResource.class);

    private static final String ENTITY_NAME = "accountGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AccountGroupService accountGroupService;

    private final AccountGroupRepository accountGroupRepository;

    private final AccountGroupQueryService accountGroupQueryService;

    public AccountGroupResource(
        AccountGroupService accountGroupService,
        AccountGroupRepository accountGroupRepository,
        AccountGroupQueryService accountGroupQueryService
    ) {
        this.accountGroupService = accountGroupService;
        this.accountGroupRepository = accountGroupRepository;
        this.accountGroupQueryService = accountGroupQueryService;
    }

    /**
     * {@code POST  /account-groups} : Create a new accountGroup.
     *
     * @param accountGroupDTO the accountGroupDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new accountGroupDTO, or with status {@code 400 (Bad Request)} if the accountGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<AccountGroupDTO> createAccountGroup(@Valid @RequestBody AccountGroupDTO accountGroupDTO)
        throws URISyntaxException {
        log.debug("REST request to save AccountGroup : {}", accountGroupDTO);
        if (accountGroupDTO.getId() != null) {
            throw new BadRequestAlertException("A new accountGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AccountGroupDTO result = accountGroupService.save(accountGroupDTO);
        return ResponseEntity
            .created(new URI("/api/account-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /account-groups/:id} : Updates an existing accountGroup.
     *
     * @param id the id of the accountGroupDTO to save.
     * @param accountGroupDTO the accountGroupDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated accountGroupDTO,
     * or with status {@code 400 (Bad Request)} if the accountGroupDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the accountGroupDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<AccountGroupDTO> updateAccountGroup(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody AccountGroupDTO accountGroupDTO
    ) throws URISyntaxException {
        log.debug("REST request to update AccountGroup : {}, {}", id, accountGroupDTO);
        if (accountGroupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, accountGroupDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!accountGroupRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AccountGroupDTO result = accountGroupService.update(accountGroupDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, accountGroupDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /account-groups/:id} : Partial updates given fields of an existing accountGroup, field will ignore if it is null
     *
     * @param id the id of the accountGroupDTO to save.
     * @param accountGroupDTO the accountGroupDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated accountGroupDTO,
     * or with status {@code 400 (Bad Request)} if the accountGroupDTO is not valid,
     * or with status {@code 404 (Not Found)} if the accountGroupDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the accountGroupDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AccountGroupDTO> partialUpdateAccountGroup(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody AccountGroupDTO accountGroupDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update AccountGroup partially : {}, {}", id, accountGroupDTO);
        if (accountGroupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, accountGroupDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!accountGroupRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AccountGroupDTO> result = accountGroupService.partialUpdate(accountGroupDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, accountGroupDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /account-groups} : get all the accountGroups.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of accountGroups in body.
     */
    @GetMapping("")
    public ResponseEntity<List<AccountGroupDTO>> getAllAccountGroups(
        AccountGroupCriteria criteria,
        @org.springdoc.core.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get AccountGroups by criteria: {}", criteria);

        Page<AccountGroupDTO> page = accountGroupQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /account-groups/count} : count all the accountGroups.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/count")
    public ResponseEntity<Long> countAccountGroups(AccountGroupCriteria criteria) {
        log.debug("REST request to count AccountGroups by criteria: {}", criteria);
        return ResponseEntity.ok().body(accountGroupQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /account-groups/:id} : get the "id" accountGroup.
     *
     * @param id the id of the accountGroupDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the accountGroupDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<AccountGroupDTO> getAccountGroup(@PathVariable("id") Long id) {
        log.debug("REST request to get AccountGroup : {}", id);
        Optional<AccountGroupDTO> accountGroupDTO = accountGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(accountGroupDTO);
    }

    /**
     * {@code DELETE  /account-groups/:id} : delete the "id" accountGroup.
     *
     * @param id the id of the accountGroupDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAccountGroup(@PathVariable("id") Long id) {
        log.debug("REST request to delete AccountGroup : {}", id);
        accountGroupService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /account-groups/_search?query=:query} : search for the accountGroup corresponding
     * to the query.
     *
     * @param query the query of the accountGroup search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search")
    public ResponseEntity<List<AccountGroupDTO>> searchAccountGroups(
        @RequestParam("query") String query,
        @org.springdoc.core.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to search for a page of AccountGroups for query {}", query);
        try {
            Page<AccountGroupDTO> page = accountGroupService.search(query, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            return ResponseEntity.ok().headers(headers).body(page.getContent());
        } catch (RuntimeException e) {
            throw ElasticsearchExceptionMapper.mapException(e);
        }
    }
}
