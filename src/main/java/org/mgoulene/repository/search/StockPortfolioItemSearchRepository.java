package org.mgoulene.repository.search;

import co.elastic.clients.elasticsearch._types.query_dsl.QueryStringQuery;
import java.util.List;
import org.mgoulene.domain.StockPortfolioItem;
import org.mgoulene.repository.StockPortfolioItemRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.scheduling.annotation.Async;

/**
 * Spring Data Elasticsearch repository for the {@link StockPortfolioItem} entity.
 */
public interface StockPortfolioItemSearchRepository
    extends ElasticsearchRepository<StockPortfolioItem, Long>, StockPortfolioItemSearchRepositoryInternal {}

interface StockPortfolioItemSearchRepositoryInternal {
    Page<StockPortfolioItem> search(String query, Pageable pageable);

    Page<StockPortfolioItem> search(Query query);

    @Async
    void index(StockPortfolioItem entity);

    @Async
    void deleteFromIndexById(Long id);
}

class StockPortfolioItemSearchRepositoryInternalImpl implements StockPortfolioItemSearchRepositoryInternal {

    private final ElasticsearchTemplate elasticsearchTemplate;
    private final StockPortfolioItemRepository repository;

    StockPortfolioItemSearchRepositoryInternalImpl(ElasticsearchTemplate elasticsearchTemplate, StockPortfolioItemRepository repository) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.repository = repository;
    }

    @Override
    public Page<StockPortfolioItem> search(String query, Pageable pageable) {
        NativeQuery nativeQuery = new NativeQuery(QueryStringQuery.of(qs -> qs.query(query))._toQuery());
        return search(nativeQuery.setPageable(pageable));
    }

    @Override
    public Page<StockPortfolioItem> search(Query query) {
        SearchHits<StockPortfolioItem> searchHits = elasticsearchTemplate.search(query, StockPortfolioItem.class);
        List<StockPortfolioItem> hits = searchHits.map(SearchHit::getContent).stream().toList();
        return new PageImpl<>(hits, query.getPageable(), searchHits.getTotalHits());
    }

    @Override
    public void index(StockPortfolioItem entity) {
        repository.findOneWithEagerRelationships(entity.getId()).ifPresent(elasticsearchTemplate::save);
    }

    @Override
    public void deleteFromIndexById(Long id) {
        elasticsearchTemplate.delete(String.valueOf(id), StockPortfolioItem.class);
    }
}
