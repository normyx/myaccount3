package org.mgoulene.repository.search;

import co.elastic.clients.elasticsearch._types.query_dsl.QueryStringQuery;
import java.util.List;
import org.mgoulene.domain.AccountGroup;
import org.mgoulene.repository.AccountGroupRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.scheduling.annotation.Async;

/**
 * Spring Data Elasticsearch repository for the {@link AccountGroup} entity.
 */
public interface AccountGroupSearchRepository extends ElasticsearchRepository<AccountGroup, Long>, AccountGroupSearchRepositoryInternal {}

interface AccountGroupSearchRepositoryInternal {
    Page<AccountGroup> search(String query, Pageable pageable);

    Page<AccountGroup> search(Query query);

    @Async
    void index(AccountGroup entity);

    @Async
    void deleteFromIndexById(Long id);
}

class AccountGroupSearchRepositoryInternalImpl implements AccountGroupSearchRepositoryInternal {

    private final ElasticsearchTemplate elasticsearchTemplate;
    private final AccountGroupRepository repository;

    AccountGroupSearchRepositoryInternalImpl(ElasticsearchTemplate elasticsearchTemplate, AccountGroupRepository repository) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.repository = repository;
    }

    @Override
    public Page<AccountGroup> search(String query, Pageable pageable) {
        NativeQuery nativeQuery = new NativeQuery(QueryStringQuery.of(qs -> qs.query(query))._toQuery());
        return search(nativeQuery.setPageable(pageable));
    }

    @Override
    public Page<AccountGroup> search(Query query) {
        SearchHits<AccountGroup> searchHits = elasticsearchTemplate.search(query, AccountGroup.class);
        List<AccountGroup> hits = searchHits.map(SearchHit::getContent).stream().toList();
        return new PageImpl<>(hits, query.getPageable(), searchHits.getTotalHits());
    }

    @Override
    public void index(AccountGroup entity) {
        repository.findOneWithEagerRelationships(entity.getId()).ifPresent(elasticsearchTemplate::save);
    }

    @Override
    public void deleteFromIndexById(Long id) {
        elasticsearchTemplate.delete(String.valueOf(id), AccountGroup.class);
    }
}
