package org.mgoulene.repository.search;

import co.elastic.clients.elasticsearch._types.query_dsl.QueryStringQuery;
import java.util.stream.Stream;
import org.mgoulene.domain.BudgetItem;
import org.mgoulene.repository.BudgetItemRepository;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.scheduling.annotation.Async;

/**
 * Spring Data Elasticsearch repository for the {@link BudgetItem} entity.
 */
public interface BudgetItemSearchRepository extends ElasticsearchRepository<BudgetItem, Long>, BudgetItemSearchRepositoryInternal {}

interface BudgetItemSearchRepositoryInternal {
    Stream<BudgetItem> search(String query);

    Stream<BudgetItem> search(Query query);

    @Async
    void index(BudgetItem entity);

    @Async
    void deleteFromIndexById(Long id);
}

class BudgetItemSearchRepositoryInternalImpl implements BudgetItemSearchRepositoryInternal {

    private final ElasticsearchTemplate elasticsearchTemplate;
    private final BudgetItemRepository repository;

    BudgetItemSearchRepositoryInternalImpl(ElasticsearchTemplate elasticsearchTemplate, BudgetItemRepository repository) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.repository = repository;
    }

    @Override
    public Stream<BudgetItem> search(String query) {
        NativeQuery nativeQuery = new NativeQuery(QueryStringQuery.of(qs -> qs.query(query))._toQuery());
        return search(nativeQuery);
    }

    @Override
    public Stream<BudgetItem> search(Query query) {
        return elasticsearchTemplate.search(query, BudgetItem.class).map(SearchHit::getContent).stream();
    }

    @Override
    public void index(BudgetItem entity) {
        repository.findOneWithEagerRelationships(entity.getId()).ifPresent(elasticsearchTemplate::save);
    }

    @Override
    public void deleteFromIndexById(Long id) {
        elasticsearchTemplate.delete(String.valueOf(id), BudgetItem.class);
    }
}
