package org.mgoulene.repository.search;

import co.elastic.clients.elasticsearch._types.query_dsl.QueryStringQuery;
import java.util.List;
import org.mgoulene.domain.StockMarketData;
import org.mgoulene.repository.StockMarketDataRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.scheduling.annotation.Async;

/**
 * Spring Data Elasticsearch repository for the {@link StockMarketData} entity.
 */
public interface StockMarketDataSearchRepository
    extends ElasticsearchRepository<StockMarketData, Long>, StockMarketDataSearchRepositoryInternal {}

interface StockMarketDataSearchRepositoryInternal {
    Page<StockMarketData> search(String query, Pageable pageable);

    Page<StockMarketData> search(Query query);

    @Async
    void index(StockMarketData entity);

    @Async
    void deleteFromIndexById(Long id);
}

class StockMarketDataSearchRepositoryInternalImpl implements StockMarketDataSearchRepositoryInternal {

    private final ElasticsearchTemplate elasticsearchTemplate;
    private final StockMarketDataRepository repository;

    StockMarketDataSearchRepositoryInternalImpl(ElasticsearchTemplate elasticsearchTemplate, StockMarketDataRepository repository) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.repository = repository;
    }

    @Override
    public Page<StockMarketData> search(String query, Pageable pageable) {
        NativeQuery nativeQuery = new NativeQuery(QueryStringQuery.of(qs -> qs.query(query))._toQuery());
        return search(nativeQuery.setPageable(pageable));
    }

    @Override
    public Page<StockMarketData> search(Query query) {
        SearchHits<StockMarketData> searchHits = elasticsearchTemplate.search(query, StockMarketData.class);
        List<StockMarketData> hits = searchHits.map(SearchHit::getContent).stream().toList();
        return new PageImpl<>(hits, query.getPageable(), searchHits.getTotalHits());
    }

    @Override
    public void index(StockMarketData entity) {
        repository.findById(entity.getId()).ifPresent(elasticsearchTemplate::save);
    }

    @Override
    public void deleteFromIndexById(Long id) {
        elasticsearchTemplate.delete(String.valueOf(id), StockMarketData.class);
    }
}
