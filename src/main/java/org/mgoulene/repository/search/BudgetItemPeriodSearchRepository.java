package org.mgoulene.repository.search;

import co.elastic.clients.elasticsearch._types.query_dsl.QueryStringQuery;
import java.util.stream.Stream;
import org.mgoulene.domain.BudgetItemPeriod;
import org.mgoulene.repository.BudgetItemPeriodRepository;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.scheduling.annotation.Async;

/**
 * Spring Data Elasticsearch repository for the {@link BudgetItemPeriod} entity.
 */
public interface BudgetItemPeriodSearchRepository
    extends ElasticsearchRepository<BudgetItemPeriod, Long>, BudgetItemPeriodSearchRepositoryInternal {}

interface BudgetItemPeriodSearchRepositoryInternal {
    Stream<BudgetItemPeriod> search(String query);

    Stream<BudgetItemPeriod> search(Query query);

    @Async
    void index(BudgetItemPeriod entity);

    @Async
    void deleteFromIndexById(Long id);
}

class BudgetItemPeriodSearchRepositoryInternalImpl implements BudgetItemPeriodSearchRepositoryInternal {

    private final ElasticsearchTemplate elasticsearchTemplate;
    private final BudgetItemPeriodRepository repository;

    BudgetItemPeriodSearchRepositoryInternalImpl(ElasticsearchTemplate elasticsearchTemplate, BudgetItemPeriodRepository repository) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.repository = repository;
    }

    @Override
    public Stream<BudgetItemPeriod> search(String query) {
        NativeQuery nativeQuery = new NativeQuery(QueryStringQuery.of(qs -> qs.query(query))._toQuery());
        return search(nativeQuery);
    }

    @Override
    public Stream<BudgetItemPeriod> search(Query query) {
        return elasticsearchTemplate.search(query, BudgetItemPeriod.class).map(SearchHit::getContent).stream();
    }

    @Override
    public void index(BudgetItemPeriod entity) {
        repository.findById(entity.getId()).ifPresent(elasticsearchTemplate::save);
    }

    @Override
    public void deleteFromIndexById(Long id) {
        elasticsearchTemplate.delete(String.valueOf(id), BudgetItemPeriod.class);
    }
}
