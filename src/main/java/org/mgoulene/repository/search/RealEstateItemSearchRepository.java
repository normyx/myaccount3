package org.mgoulene.repository.search;

import co.elastic.clients.elasticsearch._types.query_dsl.QueryStringQuery;
import java.util.List;
import org.mgoulene.domain.RealEstateItem;
import org.mgoulene.repository.RealEstateItemRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.scheduling.annotation.Async;

/**
 * Spring Data Elasticsearch repository for the {@link RealEstateItem} entity.
 */
public interface RealEstateItemSearchRepository
    extends ElasticsearchRepository<RealEstateItem, Long>, RealEstateItemSearchRepositoryInternal {}

interface RealEstateItemSearchRepositoryInternal {
    Page<RealEstateItem> search(String query, Pageable pageable);

    Page<RealEstateItem> search(Query query);

    @Async
    void index(RealEstateItem entity);

    @Async
    void deleteFromIndexById(Long id);
}

class RealEstateItemSearchRepositoryInternalImpl implements RealEstateItemSearchRepositoryInternal {

    private final ElasticsearchTemplate elasticsearchTemplate;
    private final RealEstateItemRepository repository;

    RealEstateItemSearchRepositoryInternalImpl(ElasticsearchTemplate elasticsearchTemplate, RealEstateItemRepository repository) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.repository = repository;
    }

    @Override
    public Page<RealEstateItem> search(String query, Pageable pageable) {
        NativeQuery nativeQuery = new NativeQuery(QueryStringQuery.of(qs -> qs.query(query))._toQuery());
        return search(nativeQuery.setPageable(pageable));
    }

    @Override
    public Page<RealEstateItem> search(Query query) {
        SearchHits<RealEstateItem> searchHits = elasticsearchTemplate.search(query, RealEstateItem.class);
        List<RealEstateItem> hits = searchHits.map(SearchHit::getContent).stream().toList();
        return new PageImpl<>(hits, query.getPageable(), searchHits.getTotalHits());
    }

    @Override
    public void index(RealEstateItem entity) {
        repository.findOneWithEagerRelationships(entity.getId()).ifPresent(elasticsearchTemplate::save);
    }

    @Override
    public void deleteFromIndexById(Long id) {
        elasticsearchTemplate.delete(String.valueOf(id), RealEstateItem.class);
    }
}
